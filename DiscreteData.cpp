#include "DiscreteData.h"
#include "Model_data.h"



// #include <vector>
// #include <functional>
// #include <array>
// #include <iostream>
// #include <fstream>
// #include <string>
// #include <string.h>
// #include <cmath>






using namespace std;

#ifndef sep
#define sep  "\n----------------------------------------\n----------------------------------------\n\n"
#endif


#ifndef tol_jump_quadra
#define tol_jump_quadra 2.*std::pow(10., -12)
#endif



//Edge, implementation

Edge::Edge(const int & Id,const  Point & edge_point,const Geo & geometry )
{
    m_global_id = Id;
    m_position = edge_point;
    geo = geometry;
}

int Edge::id() const
{
    return m_global_id;
}

Point Edge::position() const
{
    return m_position;
}

bool Edge::is_boundary() const
{
    return geo.is_boundary(m_position);
}

bool Edge::is_on_jump() const
{
    
    return geo.is_on_jump(m_position);
} 

NormalType Edge::normal(const Point & x) const
{
    Real vect = m_position - x;
    Real d = fabs(vect);
    if( d==0. ){ return 0;}
    else {return round (vect/d);}
}
     

NeighIdArrayType Edge::cells_id() const
{
    NeighIdArrayType vect_cells;
    if(is_boundary()){
        vect_cells.resize(1);
        if(m_global_id ==0){vect_cells[0] = 0;}
        else{vect_cells[0] =m_global_id -1; }
    } //if is boundary
    else{
        vect_cells.resize(2);
        vect_cells[0] = m_global_id -1;  
        vect_cells[1] =  m_global_id ; 
    } //else
    return vect_cells;  
}


//Cell, implementation
Cell::Cell(){} 

Cell::Cell(const int & Id, const  Point & center, const Point & left_edge ,const Point & right_edge , const Geo & geometry)
{ 
    m_global_id = Id;
    m_center = center;
    m_left_edge = left_edge;
    m_right_edge = right_edge;
    geo= geometry;
}

int Cell::id() const
{
    return m_global_id;
}

Real Cell::volume() const
{
   return m_right_edge-m_left_edge; 
}

Real Cell::size() const
{
    return m_right_edge-m_left_edge;
} 

Point Cell::center() const
{
    return m_center;
}

NeighIdArrayType Cell::edges_id() const
{
    NeighIdArrayType vect_edges;
    vect_edges.resize(2);
    vect_edges[0]= m_global_id;
    vect_edges[1]= m_global_id+1;
    return vect_edges;   
}

PointsArrayType Cell::edges_pos() const
{
    PointsArrayType vect_edges_pos;
    vect_edges_pos.resize(2);
    vect_edges_pos[0]= m_left_edge;
    vect_edges_pos[1]= m_right_edge;
    return vect_edges_pos; 
} 

int Cell::which_region() const
{
    return geo.which_reg(m_center);
} 

bool Cell::is_in(const Point & x) const
{
    return (m_left_edge<= x) && (x<= m_right_edge) ; 
} 

bool Cell::is_a_jump_cell() const //for WIAS style mesh,   
{   
    const int reg_l = geo.which_reg(m_left_edge);
    const int reg_r = geo.which_reg(m_right_edge);
    return not(reg_l == reg_r);
} 

//Mesh, implementation

Mesh::Mesh(){} 

Mesh::Mesh(const Real & size, const Geo & geometry)
{
  
    std::cout <<"Creation of the mesh (1D) : go !! \n"  <<  std::endl;

    EdgeArrayType vec_edges;
    CellArrayType vec_cells;
    int id_edge = 0;
    int id_cell = 0;
    Real max_size = 0.;
    Real min_size = size;

    geo = geometry; 
    int Nb_reg = geometry.nb_reg();
    std::cout <<  "Number of regions for the model :" << Nb_reg  <<  std::endl;
    
    Point effective_left_edge_pos = geometry.position_jump(0);
    Point effective_right_edge_pos = geometry.position_jump(0);
    vec_edges.push_back(Edge(id_edge, effective_left_edge_pos , geometry ));
    id_edge +=1;


    for(int reg = 0; reg < Nb_reg; reg ++){
        const int id_right_jump = reg +1;
        const Point right_edge_reg = geometry.position_jump(id_right_jump);
        const Point left_edge_reg = geometry.position_jump(id_right_jump-1);
        const Real lenght_reg = right_edge_reg - left_edge_reg;
        const Real local_ratio = lenght_reg / size;
        const int nb_cell_reg = ( (int) local_ratio ) + 1;
        const Real size_reg = lenght_reg / ( (Real) nb_cell_reg);
        
        max_size = std::max(max_size,size_reg); 
        min_size = std::min(min_size,size_reg);
        
        for(int cell_num = 0; cell_num < nb_cell_reg ; cell_num ++){
            effective_right_edge_pos += size_reg;
            vec_edges.push_back(Edge(id_edge, effective_right_edge_pos , geometry ));
            id_edge +=1;
            
            Point center_cell =  0.5 * (effective_right_edge_pos + effective_left_edge_pos); 
            vec_cells.push_back(Cell(id_cell, center_cell, effective_left_edge_pos, effective_right_edge_pos, geometry));
            id_cell+=1;
 
            effective_left_edge_pos = effective_right_edge_pos; 

        } // for cell_num 
    } //for reg 
    
    std::cout << "Mesh creation : DONE ! \n"  <<  std::endl;
    std::cout << "Number of cells:  " << vec_cells.size() <<  std::endl;
    std::cout << "Number of edges:  " << vec_edges.size() <<  std::endl;
    std::cout << "Mesh size:  " << max_size <<  std::endl;
    std::cout << "Size of the smaller cell: "  <<  min_size <<  std::endl;
    std::cout << sep <<  std::endl;

    m_edges = vec_edges;
    m_cells = vec_cells; 

    m_size = max_size;
    m_min_size = min_size;
    
    set_diamond_cells();
}


Mesh::Mesh(const CellArrayType & listCells, const Geo & geometry)
{
    std::cout <<"Creation of the dual mesh (1D) : go !! \n"  <<  std::endl;

    m_cells = listCells;
    const int nb_cells = listCells.size();

    std::cout << "Number of cells:  " << nb_cells <<  std::endl;
    geo = geometry;
    
    EdgeArrayType vec_edges;
    int id_edge = 0;
    
    const Cell D_0 = listCells[0]; 
    vec_edges.push_back(Edge(id_edge,D_0.edges_pos()[0],geometry));
    id_edge +=1;  
    
    Real max_size = D_0.edges_pos()[1]-D_0.edges_pos()[0];
    Real min_size = D_0.edges_pos()[1]-D_0.edges_pos()[0];
    
    for(int iK=0; iK < nb_cells; iK ++){
        const Cell D = listCells[iK]; 
        vec_edges.push_back(Edge(id_edge,D.edges_pos()[1],geometry));
        id_edge +=1;  
        Real size_D = D.edges_pos()[1]-D.edges_pos()[0];      
        max_size = std::max(max_size,size_D); 
        min_size = std::min(min_size,size_D);
    } //for iK 
    
    std::cout << "Mesh creation : DONE ! \n"  <<  std::endl;
    std::cout << "Number of edges:  " << vec_edges.size() <<  std::endl;
    std::cout << "Mesh size:  " << max_size <<  std::endl;
    std::cout << "Size of the smaller cell: "  <<  min_size <<  std::endl;
    std::cout << sep <<  std::endl;

    m_edges = vec_edges;

    m_size = max_size;
    m_min_size = min_size;
    
    set_diamond_cells();

}

Geo Mesh::get_geometry() const
{
    return geo;
} 

Point Mesh::get_left_boundary_pos() const
{
    return ( (m_cells[0]).edges_pos())[0];
}

Point Mesh::get_right_boundary_pos() const
{
    return ( (m_cells[number_cells()-1]).edges_pos())[1];
}

Real Mesh::size() const
{
 return m_size;
}     

Real Mesh::minimal_size() const
{
    return m_min_size;
}

int Mesh::number_edges() const
{
    return m_edges.size();
} 

int Mesh::number_cells() const
{
    return m_cells.size();
} 

Cell Mesh::cell(const int & iT) const
{
    return m_cells[iT];
}

Edge Mesh::edge(const int & iE) const 
{
    return m_edges[iE];
}

Cell Mesh::diamond(const int & iE) const
{
    return m_diamond_cells[iE];
}

std::vector<Cell> Mesh::neighboring_cells(const int & iE) const
{
    CellArrayType neigh_cells;
    if(iE ==0){
        neigh_cells.resize(1);  
        neigh_cells[0] = cell(0);
    }// if left boundary edge
    else if(iE == number_edges()-1){
        neigh_cells.resize(1);  
        neigh_cells[0] = cell(number_cells()-1);
    }// if right boundary edge 
    else{   
        neigh_cells.resize(2);  
        neigh_cells[0] = cell(iE-1);
        neigh_cells[1] = cell(iE);
    }
    return neigh_cells;
}
    
bool Mesh::idx_Ks_isboundary(const int & iK, const int & iE) const
{
    if((iE==0) || (iE== number_edges() - 1) ){ return true ; }
    else{ return false ; }
}

int Mesh::cell_idx_Ks(const int & iK, const int & iE) const
{   
    const int check = iE-iK;
    if(not( (check ==0) || (check == 1))){SolutionVectorType TP_interpolant(const ReconstructionType & fct , const Mesh & msh, const int & order);
        std::cout <<"BIG WARNING !!!! \n"  <<  std::endl;
        std::cout <<"You want to use a Ks with s non in E_K !!! \n"  <<  std::endl;
        std::cout <<"We return a arbitrary big value -> Bugs are comming \n"  <<  std::endl;
        return number_cells()*( (int) pow(10,5));
    }
    return iK + edge(iE).normal(cell(iK).center());
}


void Mesh::set_diamond_cells()
{
    CellArrayType vec_diam;
    int N_edge= number_edges();
    
    Edge sigma_0 = edge(0);
    NeighIdArrayType cells_0 = sigma_0.cells_id();
    Cell cell_0=cell(cells_0[0]);
    vec_diam.push_back(Cell(0, 0.5 * (sigma_0.position() +cell_0.center()) , sigma_0.position(), cell_0.center(), geo));

    for(int iE = 1; iE < N_edge-1; iE ++ ){
        Edge sigma = edge(iE);
        NeighIdArrayType cells = sigma.cells_id();
        vec_diam.push_back(Cell(iE,sigma.position(), cell(cells[0]).center(), cell(cells[1]).center(), geo));
    }

    Edge sigma_l = edge(N_edge-1);
    NeighIdArrayType cells_l = sigma_l.cells_id();
    Cell cell_l=cell(cells_l[0]);
    vec_diam.push_back(Cell(N_edge-1, 0.5 * (sigma_l.position() +cell_l.center()) , cell_l.center(), sigma_l.position(), geo));

    m_diamond_cells = vec_diam; 
}

bool Mesh::is_in_cell(const Point & x, const int iT) const
{
    return cell(iT).is_in(x);
} 

int Mesh::which_cell(const Point & x) const
{
    for(int iT = 0; iT < number_cells() ;  iT ++ ){
        if(is_in_cell( x, iT)){return iT; }
    } // for iT
}

bool Mesh::is_in_diamond(const Point & x, const int iE) const
{   
    Cell Diam = m_diamond_cells[iE]; 
    return Diam.is_in(x);
} 

int Mesh::which_diamond(const Point & x) const
{
    for(int iE = 0; iE < number_edges() ;  iE ++ ){
        if(is_in_diamond( x, iE)){return iE; }
        } // for iE
} 

Real Mesh::d_edge(const int & iE) const
{   
    CellArrayType neigh_cells = neighboring_cells(iE);
    const int nb_neigh = neigh_cells.size();
    if(nb_neigh == 1){
        Edge sigma = edge(iE);
        return fabs(sigma.position() - neigh_cells[0].center() ); 
    }
    else{ return fabs(neigh_cells[0].center()-neigh_cells[1].center());}
} 

void Mesh::file_edge_pos(const std::string & name) const
{
    int nb_edge = number_edges();
    std::ofstream myfile;
    myfile.open (name );
    myfile << "Position edge \n";
    for(int n=0; n< nb_edge; n ++){
        Point pos = m_edges[n].position();
        myfile << pos << " " << 0 << endl;
    }//for n
    myfile.close();
} 


void Mesh::file_cell_pos(const std::string & name) const
{
    int nb_cell = number_cells();
    std::ofstream myfile;
    myfile.open (name);
    myfile << "Position cell_center \n";
    for(int n=0; n< nb_cell; n ++){
        Point center = m_cells[n].center();
        myfile << center << " " << 0 << endl;
    }//for n
    myfile.close();
} 

void Mesh::file_diamond_center_pos(const std::string & name) const
{
    int nb_edge = number_edges();
    std::ofstream myfile;
    myfile.open ( name );
    myfile << "Position diam_center \n";
    for(int n=0; n< nb_edge; n ++){
        Point center = m_diamond_cells[n].center();
        myfile << center << " " << 0 << endl;
    }//for n
    myfile.close();
}

// Creation of dual meshes

Mesh get_dual_mesh(const Mesh & msh)
{
    typedef std::vector<Cell> CellArrayType;
    CellArrayType ListDiams;
    const int nb_Diam = msh.number_edges();
    for(int iD = 0; iD < nb_Diam; iD ++){
        ListDiams.push_back(msh.diamond(iD));
    } 
    return Mesh(ListDiams,msh.get_geometry());
}


// Creation of a regularized energy band profile, using a given mean fonction for the interfaces

ReconstructionType get_E_WIAS_style(const BandEdgeType & E, const Mesh & dualmsh,  const int & order)
{
    ReconstructionType E_reg;
    
    E_reg =[E,dualmsh,order](const Point & x) -> Real {
        Real E_K=0.;
        const int iK = dualmsh.which_cell(x);
        const Cell K = dualmsh.cell(iK); 
        const Point x_l = K.edges_pos()[0];
        const Point x_r = K.edges_pos()[1];
        // if(x_r>= dualmsh.get_right_boundary_pos()){
        //     E_K=E(dualmsh.get_right_boundary_pos());
        // }
        // else if(x_l <= dualmsh.get_left_boundary_pos()){
        //     E_K=E(dualmsh.get_left_boundary_pos());
        // }
        // else 
        if(K.is_a_jump_cell()){
            const Point x_m = K.center();
            const Real I_l = integral_approx(E,x_l,x_m-tol_jump_quadra,order); 
            const Real I_r = integral_approx(E,x_m + tol_jump_quadra,x_r,order);
            E_K = (I_l+I_r) / K.volume();
        }
        else{
            E_K = mean_val_approx( E, x_l, x_r, order);
        }
        return E_K;
    };
    return E_reg; 
}

// Discrete_unkw, implementation 
Discrete_u::Discrete_u(): m_mesh(Mesh()), m_nunkw(1), m_left_boundary_val(0.), m_right_boundary_val(0.)
        , m_uknw(SolutionVectorType::Zero(1)) {}


Discrete_u::Discrete_u(const Discrete_u & u):
    m_mesh(u.m_mesh), m_nunkw(u.m_nunkw), m_left_boundary_val(u.m_left_boundary_val), m_right_boundary_val(u.m_right_boundary_val) , m_uknw(u.m_uknw) {}


Discrete_u::Discrete_u(const Real & left_val, const Real & right_val, const Mesh & M) 
    : m_mesh(M), m_nunkw(M.number_cells()), m_left_boundary_val(left_val), m_right_boundary_val(right_val)
        , m_uknw(SolutionVectorType::Zero(M.number_cells())) {}

    // m_mesh = M;
    // const int n_vol = M.number_cells();
    // m_uknw = SolutionVectorType::Zero(n_vol);
    
    // m_left_boundary_val = left_val;
    // m_right_boundary_val = right_val;
    // m_nunkw = n_vol; 


Discrete_u::Discrete_u(const LiftingBoundaryType & u_b, const Mesh & M)
{
    m_mesh = M;
    const int n_vol = M.number_cells();
    m_uknw = SolutionVectorType::Zero(n_vol);
    
    m_left_boundary_val = u_b ( (M.edge(0)).position() );
    m_right_boundary_val = u_b ( (M.edge(M.number_edges()-1)).position() ); 
    m_nunkw = n_vol; 
}

Discrete_u::Discrete_u( const Mesh & M )
{
    m_mesh = M;
    const int n_vol = M.number_cells();
    m_uknw = SolutionVectorType::Ones(n_vol);
    
    m_left_boundary_val = 1.;
    m_right_boundary_val = 1.; 
    m_nunkw = n_vol; 
}

int Discrete_u::nunkw() const
{
    return m_nunkw;
} 

void Discrete_u::set_from_values(const SolutionVectorType & U)
{
    m_uknw = U; 
} 

void Discrete_u::set_from_triplets(const ListTripletType & Triplets, const SolutionVectorType & RHS)
{    

    SparseMatrixType Mat(m_nunkw,m_nunkw);
    Mat.setFromTriplets(Triplets.begin(), Triplets.end());
    SolverType solver;
    // Compute the ordering permutation vector from the structural pattern of A
    solver.analyzePattern(Mat);
    solver.factorize(Mat);
    m_uknw = solver.solve(RHS);
}  

void Discrete_u::set_from_solver(const SolverType & Solver, const SolutionVectorType & RHS)
{
    m_uknw = Solver.solve(RHS);
}

void Discrete_u::set_from_fonction(const ReconstructionType & u_cont, const int & order)
{   
    int effective_order = order;  
    if(order==2){
        std::cout <<"WARNING : you asked for a interpolation of order" << order <<" which is not implemented (yet ?) !! \n"  <<  std::endl;
        std::cout <<"The interpolation used is of order 1 (barycentric value) instead \n"  <<  std::endl;
        effective_order = 1 ;
    }
    if(order > 3 ){
        std::cout <<"WARNING : you asked for a interpolation of order" << order <<" which is not implemented (yet ?) !! \n"  <<  std::endl;
        std::cout <<"The interpolation used is of order 3 (Simpson formula) instead \n"  <<  std::endl;
        effective_order = 3 ;
    }       
    
    for(int iK = 0; iK< m_nunkw; iK ++){
        const Cell K = m_mesh.cell(iK);
        const Point x_K = K.center();
        const Point x_l = K.edges_pos()[0];
        const Point x_r = K.edges_pos()[1];
        Real u_K = 0.;
        if(effective_order == 0){ u_K = u_cont(x_K);}
        else { u_K = mean_val_approx(u_cont,x_l,x_r,effective_order);}
        m_uknw(iK) = u_K;
    } // for iT
}

void Discrete_u::set_from_discontinuous_fonction(const ReconstructionType & u_cont, const int & order)  
{
    int effective_order = order;  
    if(order==2){
        std::cout <<"WARNING : you asked for a interpolation of order" << order <<" which is not implemented (yet ?) !! \n"  <<  std::endl;
        std::cout <<"The interpolation used is of order 1 (barycentric value) instead \n"  <<  std::endl;
        effective_order = 1 ;
    }
    if(order > 3 ){
        std::cout <<"WARNING : you asked for a interpolation of order" << order <<" which is not implemented (yet ?) !! \n"  <<  std::endl;
        std::cout <<"The interpolation used is of order 3 (Simpson formula) instead \n"  <<  std::endl;
        effective_order = 3 ;
    }       
    
    for(int iK = 0; iK< m_nunkw; iK ++){
        const Cell K = m_mesh.cell(iK);
        const Point x_K = K.center();
        const Point x_l = K.edges_pos()[0];
        const Point x_r = K.edges_pos()[1];
        Real u_K = 0.;
        if(K.is_a_jump_cell()){
            if(effective_order == 0){ 
                u_K = 0.5 * (u_cont(0.5 * (x_l + x_K)) + u_cont(0.5 * (x_K + x_r)) );
            }
            else { 
                u_K = integral_approx(u_cont,x_l,x_K-tol_jump_quadra,effective_order) +  integral_approx(u_cont,x_K + tol_jump_quadra,x_r,effective_order); 
                u_K = u_K/K.volume(); 
            }
        }
        else{
            if(effective_order == 0){ u_K = u_cont(x_K);}
            else { u_K = mean_val_approx(u_cont,x_l,x_r,effective_order);}
        }
        m_uknw(iK) = u_K;
    } // for iT
}

void Discrete_u::set_boundary_values(const Real & left_val, const Real & right_val)
{
    m_left_boundary_val = left_val;
    m_right_boundary_val = right_val;
}


Discrete_u& Discrete_u::operator+=(const Discrete_u & V)
{
    m_left_boundary_val += V.m_left_boundary_val;
    m_right_boundary_val += V.m_right_boundary_val;
    m_uknw += V.m_uknw;
    return *this;
}

Discrete_u& Discrete_u::operator-=(const Discrete_u & V)
{
    m_left_boundary_val += -V.m_left_boundary_val;
    m_right_boundary_val += -V.m_right_boundary_val;
    m_uknw += -V.m_uknw;
    return *this; 
}

Discrete_u&  Discrete_u::operator*=(const Discrete_u & V)
{
    m_left_boundary_val *= V.m_left_boundary_val;
    m_right_boundary_val *= V.m_right_boundary_val;
    m_uknw *= V.m_uknw;
    return *this;   
}

Discrete_u&  Discrete_u::operator/=(const Discrete_u & V)
{
    m_left_boundary_val /= V.m_left_boundary_val;
    m_right_boundary_val /= V.m_right_boundary_val;
    for(int iK = 0; iK< m_nunkw; iK++){
        m_uknw(iK) /= V.m_uknw(iK);
    }
    return *this;  
}

Discrete_u& Discrete_u::operator*=(const Real &  alpha )
{
    m_left_boundary_val *= alpha;
    m_right_boundary_val *= alpha;
    m_uknw *= alpha;
    return *this;   

}


void Discrete_u::Proj_on_Ih_eps(const Statistics & stat, const Real & eps)
{
    m_uknw = Projection_u(m_uknw , stat, eps); 
}


void Discrete_u::Apply_function(const RealFctType & fct)
{
    m_left_boundary_val = fct(m_left_boundary_val);
    m_right_boundary_val =  fct(m_right_boundary_val);
    for(int iK = 0; iK < m_nunkw; iK++){
        m_uknw(iK) = fct(cell_value(iK));
    } // for iK 
}

Real Discrete_u::cell_value(const int & Id) const
{   
    if(Id<0){return m_left_boundary_val;}
    else if(Id >m_nunkw-1){return m_right_boundary_val;}
    else{return m_uknw(Id);}
}

SolutionVectorType Discrete_u::cells_values() const
{
    return m_uknw;
}

Real Discrete_u::Ks_value(const int & iK, const int & iE) const
{
    const Edge sigma = m_mesh.edge(iE);
    const NormalType val_n = sigma.normal(m_mesh.cell(iK).center());
    return cell_value(iK + val_n);
   
}

Real Discrete_u::D_Ks(const int & iK, const int & iE) const
{      
    return Ks_value(iK,iE)-cell_value(iK);
}

Real Discrete_u::D_s( const int & iE) const
{   
    return fabs(cell_value(iE-1)-cell_value(iE)); // bof, 
}


Real Discrete_u::L2_square_norm() const
{
    Real l2_square = 0.;
    for(int iK =0; iK < m_nunkw; iK++){
        const Cell K = m_mesh.cell(iK);
        l2_square+= K.size() *std::pow( m_uknw(iK), 2);
    }
    return l2_square; 
}

Real Discrete_u::false_L2_square_norm() const
{
    Real l2_square = 0.;
    for(int iK =0; iK < m_nunkw; iK++){
        const Cell K = m_mesh.cell(iK);
        if(not(K.is_a_jump_cell())){ l2_square+= K.size() *std::pow( m_uknw(iK), 2);}
    }
    return l2_square; 
}

Real Discrete_u::H1_square_norm() const
{
    Real h1_square = 0.;
    for(int iE =0; iE < m_mesh.number_edges(); iE++){
        h1_square+= D_s(iE)*D_s(iE) / m_mesh.d_edge(iE);
    }
    return h1_square; 
}

Real Discrete_u::L_infty_norm() const
{
    return m_uknw.lpNorm<Eigen::Infinity>();
}

void Discrete_u::print_values_debug() const
{
    std::cout <<"Values of the discrete vector [debug] \n"  <<  std::endl;
    std::cout <<"Boundary values : "  << m_left_boundary_val << "  and  "  << m_right_boundary_val <<  std::endl;
    std::cout <<"Cells values : "  <<  std::endl;
    for(int iK =0; iK < m_nunkw; iK++){
        std::cout <<"Cells n°"<< iK <<": \t " << cell_value(iK) <<  std::endl;
    }
    std::cout <<"That's all [end debug] \n"  <<  std::endl;
} 

ReconstructionType Discrete_u::Recons(const int & order ) const
{
    ReconstructionType u_M;
    if(order == 0){ 
        u_M =[this](const Point & x) -> Real {
            int iT = this->m_mesh.which_cell(x);  
            return this->cell_value(iT);
        };
    }
    else if (order == 1){ 
        //ReconstructionType u_M0 =  Recons(0);

        u_M =[this](const Point & x) -> Real {
            int iD = this->m_mesh.which_diamond(x);
            Cell Diam = m_mesh.diamond(iD);
            const Real l_diam = Diam.size();
            const Point x_l = Diam.edges_pos()[0];

            // const Point x_r = Diam.edges_pos()[1];
            // Real u_l = 0.;
            // Real u_r = 0.;
            // if(iD == 0){
            //     u_l = m_left_boundary_val;
            //     u_r = this->Recons(0)(x_r);
            // } // if iD == 0; left boundary
            // else if(iD == m_mesh.number_edges()-1){
            //     u_l = this->Recons(0)(x_l);
            //     u_r = m_right_boundary_val;
            // } // if iD == nb_edge-1, right boundary
            // else{
            //     u_l = this->Recons(0)(x_l);
            //     u_r = this->Recons(0)(x_r);
            // }            
            //NeighIdArrayType edges_id =  Diam.edges_id();
            Real u_l = cell_value(iD-1);
            Real u_r = cell_value(iD);
            const Real d_u= (u_r-u_l)/l_diam;
            return  u_l + (x-x_l)*d_u;
        }; 
    }
    else{
        std::cout <<"WARNING : you asked for a reconstruction of order" << order <<" which is not implemented (yet ?) !! \n"  <<  std::endl;
        std::cout <<"The reconstruction used is of order 1 instead \n"  <<  std::endl;
        return Recons(1);
    }
    return u_M; 
} 

void Discrete_u::file_visu_cst(const std::string & name) const
{
    std::ofstream myfile;
    myfile.open (name);
    myfile << "x val_u \n";
    myfile << m_mesh.edge(0).position() << " " << cell_value(-1) << endl;
    for(int iK=0; iK< m_nunkw; iK ++){
        Point x_K = (m_mesh.cell(iK)).center();
        myfile << x_K << " " << m_uknw(iK) << endl;
    }//for iK
    myfile << m_mesh.edge(m_nunkw).position() << " " << cell_value(m_nunkw) << endl;
    myfile.close();    
}


void Discrete_u::file_visu_recons(const std::string & name, const int  & order, const int & number_point) const
{   
    const ReconstructionType u_M = Recons(order);

    const Point e_l = (m_mesh.edge(0)).position();
    const Point e_r = (m_mesh.edge(m_mesh.number_edges()-1)).position();
    const Real h_visu = (e_r-e_l) / number_point; 
    Point x = e_l;
    std::ofstream myfile;
    myfile.open (name);
    myfile << "x val_u \n";

    for(int i=0; i< number_point ; i ++){
        myfile << x+ i * h_visu << " " << u_M(x + i * h_visu ) << endl;
    }//for i
    myfile.close(); 
}

Discrete_u operator+(const  Discrete_u& U, const  Discrete_u & V)
{
    Discrete_u W(U);
    W+=V;
    return W;
}

Discrete_u operator-(const  Discrete_u& U, const  Discrete_u & V)
{
    Discrete_u W(U);
    W-=V;
    return W;
}

Discrete_u operator*(const  Discrete_u& U, const  Discrete_u & V)
{
    Discrete_u W(U);
    W*=V;
    return W;
}

Discrete_u operator/(const  Discrete_u& U, const  Discrete_u & V)
{
    Discrete_u W(U);
    W/=V;
    return W;
}


Discrete_u operator*(const Real & alpha , const  Discrete_u& U)
{
    Discrete_u W(U);
    W*=alpha;
    return W;
}


Discrete_u::~Discrete_u(){}



SolutionVectorType TP_interpolant(const ReconstructionType & fct , const Mesh & msh, const int & order)
{
    const int N = msh.number_cells(); 
    SolutionVectorType Interpolant = SolutionVectorType::Zero(N);
    for(int iK =0; iK < N; iK++){
        const Cell K = msh.cell(iK);
        Interpolant(iK) = mean_val_approx(fct,K.edges_pos()[0],K.edges_pos()[1], order );
    }
    return Interpolant;
}

SolutionVectorType Projection_u(const SolutionVectorType & S, const Statistics & stat, const Real & s_proj )
{   
    const int N = S.size(); 
    SolutionVectorType proj = SolutionVectorType::Zero(N);
    
    const int nb_bounds = stat.nb_bounds;
    const std::vector<Real> bounds = stat.Bounds;

    if(nb_bounds == 1){ 
        const Real val_min = bounds[0] + s_proj;
        for(int iK =0; iK < N; iK++){
            proj(iK) = max(S(iK),val_min );
        }// for iK
    } // if one bounds 

    else if(nb_bounds == 2){
        const Real val_min = bounds[0] + s_proj;
        const Real val_max = bounds[1] - s_proj;
        for(int iK =0; iK < N; iK++){
            proj(iK) = min(val_max,max(S(iK),val_min ));
        }// for iK
    } //if two bounds
    return proj;

}
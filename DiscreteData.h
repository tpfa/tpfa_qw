#ifndef DISCRETEDATA_H
#define DISCRETEDATA_H

#include "Model_data.h"

//Types

typedef std::vector<int> NeighIdArrayType;
typedef int NormalType;
typedef Eigen::Matrix<Real, Eigen::Dynamic, 1> SolutionVectorType;
typedef Eigen::SparseMatrix<Real> SparseMatrixType;
typedef Eigen::SparseLU<SparseMatrixType> SolverType;
typedef Eigen::Triplet<Real> TripletType;
typedef std::vector<TripletType> ListTripletType;
typedef std::function<Real(const Point &)> ReconstructionType;





class Edge
{   
    public:
    Edge(const int & Id,const Point & edge_point,const Geo & geometry );
    int id() const;
    Point position() const;
    bool is_boundary() const;
    bool is_on_jump() const; 
    NormalType normal(const Point & x) const;
    NeighIdArrayType cells_id() const;
    

    private:
    int m_global_id;
    Point m_position;
    Geo geo;

};


class Cell
{   
    public:
    Cell();
    Cell(const int & Id,const Point & center, const Point & left_edge ,const Point & right_edge ,const Geo & geometry); 
    int id() const; 
    Real volume() const;
    Real size() const; 
    Point center() const;
    NeighIdArrayType edges_id() const;
    PointsArrayType edges_pos() const; 
    int which_region() const;
    bool is_in(const Point & x) const; 
    
    bool is_a_jump_cell() const; //for WIAS style meshes


    private:
    int m_global_id;
    Point m_center;
    Point m_left_edge;
    Point m_right_edge;
    Geo geo;
};



class Mesh
{   
    typedef std::vector<Edge> EdgeArrayType;
    typedef std::vector<Cell> CellArrayType;


    public:
    Mesh();
    Mesh(const Real & size, const Geo & geometry);
    Mesh(const CellArrayType & listCells, const Geo & geometry);

    Geo get_geometry() const; 

    Point get_left_boundary_pos() const; 
    Point get_right_boundary_pos() const; 
    
    Real size() const;  
    Real minimal_size() const; 
    int number_edges() const; 
    int number_cells() const; 
    Cell cell(const int & iT) const ;
    Edge edge(const int & iE) const ;
    Cell diamond(const int & iE) const ;

    CellArrayType neighboring_cells(const int & iE) const;

    bool idx_Ks_isboundary(const int & iK, const int & iE) const; 
    int cell_idx_Ks(const int & iK, const int & iE) const; 
    

    void set_diamond_cells();

    bool is_in_cell(const Point & x, const int iT) const; 
    int which_cell(const Point & x) const ; 
    
    bool is_in_diamond(const Point & x, const int iE) const; 
    int which_diamond(const Point & x) const ; 
    
    Real d_edge(const int & iE) const; 
    
    
    void file_edge_pos(const std::string & name) const; 
    void file_cell_pos(const std::string & name) const;
    void file_diamond_center_pos(const std::string & name) const;  



    private:
    Real m_size;
    Real m_min_size;
    EdgeArrayType m_edges;
    CellArrayType m_cells;
    
    CellArrayType m_diamond_cells;
    Geo geo;
};

Mesh get_dual_mesh(const Mesh & msh); //create the dual mesh (~ diamond mesh) associated to msh
ReconstructionType get_E_WIAS_style(const BandEdgeType & E, const Mesh & dualmsh, const int & order); //create the regularized energy profile 


class Discrete_u
{
    public: 
    Discrete_u();

    Discrete_u(const Discrete_u & u);

    Discrete_u(const Real & left_val, const Real & right_val, const Mesh & M ); // construction of a "zero" unknown with given boundary values  
    Discrete_u(const LiftingBoundaryType & u_b, const Mesh & M ); // construction of a "zero" unknown with boundary values given by a lifting
    Discrete_u(const Mesh & M ); // construction of a constant (=1) unknown
    
    int nunkw() const; 
    
    void set_from_values(const SolutionVectorType & U); // construction from precomputed values
    void set_from_triplets(const ListTripletType & Triplets, const SolutionVectorType & RHS);  // solution from a linear system
    void set_from_solver(const SolverType & Solver, const SolutionVectorType & RHS);  // solution from a prefactorized linear system
    void set_from_fonction(const ReconstructionType & u_cont, const int & order); // construction from a continuous fonction    
    void set_from_discontinuous_fonction(const ReconstructionType & u_cont, const int & order); // construction from a discontinuous fonction, with discontinuities on the interfaces    

    void set_boundary_values(const Real & left_val, const Real & right_val);
    
    Discrete_u& operator+=(const Discrete_u & V);
    Discrete_u& operator-=(const Discrete_u & V);
    Discrete_u& operator*=(const Discrete_u & V);
    Discrete_u& operator/=(const Discrete_u & V);
    
    Discrete_u& operator*=(const Real &  alpha );

    void Proj_on_Ih_eps(const Statistics & stat, const Real & eps);

    void Apply_function(const RealFctType & fct);

    Real cell_value(const int & Id) const; //if -1 or Nb_cells, boundary values ! 
    SolutionVectorType cells_values() const; 
    
    Real Ks_value(const int & iK, const int & iE) const;
    Real D_Ks(const int & iK, const int & iE) const; 
    Real D_s( const int & iE) const; 
    Real L2_square_norm() const;
    Real false_L2_square_norm() const; // for WIAS strategy : do not account of the jump values
    Real H1_square_norm() const;
    Real L_infty_norm() const; 

    void print_values_debug() const; 

    ReconstructionType Recons(const int & order) const;    
    void file_visu_cst(const std::string & name) const; 
    void file_visu_recons(const std::string & name, const int  & order, const int & number_point) const; 

    ~Discrete_u();

    private:
    Mesh m_mesh;
    int m_nunkw;
    Real m_left_boundary_val; 
    Real m_right_boundary_val; 
    SolutionVectorType m_uknw; 
};

Discrete_u operator+(const  Discrete_u& U, const  Discrete_u& V);
Discrete_u operator-(const  Discrete_u& U, const  Discrete_u& V);  
Discrete_u operator*(const  Discrete_u& U, const  Discrete_u& V); 
Discrete_u operator/(const  Discrete_u& U, const  Discrete_u& V); 

Discrete_u operator*(const Real & alpha , const  Discrete_u& U); 


SolutionVectorType TP_interpolant(const ReconstructionType & fct , const Mesh & msh, const int & order);

SolutionVectorType Projection_u(const SolutionVectorType & S, const Statistics & stat, const Real & s_proj );

#endif

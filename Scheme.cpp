#include "Scheme.h"

#include <chrono>

using namespace std;

#ifndef s_regul
#define s_regul 0.5*std::pow(10., -4)
#endif

#ifndef s_proj
#define s_proj 1.*std::pow(10., -11)
#endif

#ifndef goal_residue_size
#define goal_residue_size 5.*std::pow(10., -9)
#endif

#ifndef max_iter
#define max_iter  50
#endif

#ifndef sep
#define sep  "\n----------------------------------------\n----------------------------------------\n\n"
#endif

std::function<Real(const Real &)> exp_fct = [] (const Real & s) -> Real { return std::exp(s);};

std::function<Real(const Real &)> log_fct = [] (const Real & s) -> Real { return std::log(s);};



//////  Functions (data) for the scheme, implementation

//B_fct (B-scheme), implementation 

B_Fct::B_Fct() // default constructor : Bernouilli (SG) function
{
        RealFctType B;
        RealFctType dB;
        B =[this](const Real & x) -> Real {
            if(fabs(x)>s_regul){ 
            return x /(exp(x) -1.); 
            }
            else{  
                Real b = 1. - 0.5 * x;
                Real x_2 = x*x;
                b+=  x_2/12.;
                x_2 *=x_2 ;
                b+= x_2/720.;
                return b;
            }
        };    

        dB =[this](const Real & x) -> Real {
            if(fabs(x)>s_regul){ 
            
                const Real e_x = exp(x);
                Real denum = e_x-1.;
                denum *= denum;
                const Real num = e_x * ( x-1. ) + 1.;
                return  - num/denum;  
            }
            else{  
                Real db = - 0.5 +  x/6.;
                Real x_2 = x*x;
                 Real ho = -(1./180.) + x_2 / 5040.;   
                 ho *= x * x_2;
                 db += ho;
                //db += - (x_2*x) / 180.;
                return db;
            }
        };
        m_B_func = B;
        m_dB_func = dB;       

}

B_Fct::B_Fct(const std::string & type_B)
{
    if(type_B == "B_harmo")
    {
        RealFctType B;
        RealFctType dB;
        B =[this](const Real & x) -> Real {
            return 2. /(exp(x) +1.);
        };
        dB =[this](const Real & x) -> Real {
            const Real e_x = exp(x);
            Real denom = e_x+1.;
            denom *= denom;
            return -2.*e_x  /denom;
        };
        m_B_func = B;
        m_dB_func = dB;        
    }
    else if(type_B =="SG")
    {
        RealFctType B;
        RealFctType dB;
        B =[this](const Real & x) -> Real {
            if(fabs(x)>s_regul){ 
            return x /(exp(x) -1.); 
            }
            else{  
                Real b = 1. - 0.5 * x;
                Real x_2 = x*x;
                b+=  x_2/12.;
                x_2 *=x_2 ;
                b+= x_2/720.;
                return b;
            }
        };    

        dB =[this](const Real & x) -> Real {
            if(fabs(x)>s_regul){ 
            
                const Real e_x = exp(x);
                Real denum = e_x-1.;
                denum *= denum;
                const Real num = e_x * ( x-1. ) + 1.;
                return  - num/denum;  
            }
            else{  
                Real db = - 0.5 +  x/6.;
                Real x_2 = x*x;
                // Real ho = -(1./180.) + x_2 / 5040.;   
                // ho *= x * x_2;
                // db += ho;
                db += - (x_2*x) / 180.;
                return db;
            }
        };
        m_B_func = B;
        m_dB_func = dB;   
        
    }
    else{
        std::cout <<"You asked for a unknown type of B function \n"  <<  std::endl;
        std::cout <<"SG type will be used instead \n"  <<  std::endl;
        RealFctType B;
        RealFctType dB;
        B =[this](const Real & x) -> Real {
            if(fabs(x)>s_regul){ 
            return x /(exp(x) -1.); 
            }
            else{  
                Real b = 1. - 0.5 * x;
                Real x_2 = x*x;
                b+=  x_2/12.;
                x_2 *=x_2 ;
                b+= x_2/720.;
                return b;
            }
        };    

        dB =[this](const Real & x) -> Real {
            if(fabs(x)>s_regul){ 
            
                const Real e_x = exp(x);
                Real denum = e_x-1.;
                denum *= denum;
                const Real num = e_x * ( x-1. ) + 1.;
                return  - num/denum;  
            }
            else{  
                Real db = - 0.5 +  x/6.;
                Real x_2 = x*x;
                // Real ho = -(1./180.) + x_2 / 5040.;   
                // ho *= x * x_2;
                // db += ho;
                db += - (x_2*x) / 180.;
                return db;
            }
        };
        m_B_func = B;
        m_dB_func = dB;    
    }
}


Real B_Fct::B(const Real & x) const
{
    return m_B_func(x);
}

Real B_Fct::dB(const Real & x) const
{
    return m_dB_func(x);
}

void B_Fct::debug_test_diff( const int &  nb_point)
{
    std::cout << "\n"<< std::endl;
    std::cout << "Performing a test of the differential for the B function a la Gaudeul "<< std::endl;
    std::cout << "May it be a success ... " << std::endl;
    //const Real epsilon = 0.0000001;
    std::srand(std::time(nullptr));

    cout << "New base point. "<< endl;
    for(int nb_base = 0; nb_base < nb_point; nb_base ++){
        cout << "Base point number "<< nb_base +1 << endl;

        const Real x_base =  0.00001*( (Real)rand()/ ((Real)rand() ) ) ; //( (Real)rand()/ ((Real) RAND_MAX) ) / 100000.;
        cout << "Base point: \t "<< x_base << endl;
        const Real dB_base = dB(x_base);

        cout << "New direction !"<< endl;
        for(int nb_dir = 0; nb_dir <3; nb_dir++){
            cout << "Direction number "<< nb_dir +1 << endl;
            cout << "   " << endl;

            Real x_dir = ( (Real)( rand()/(Real) RAND_MAX) ) - 0.5;
            x_dir = x_dir/fabs(x_dir);
            cout << "Direction value: \t "<< x_dir << endl;



            for(int nb_h = 1; nb_h < 8; nb_h ++){
                const Real h = 1. * std::pow(10.,  -nb_h);
             
                    
                const Real x= x_base + h * x_dir;


                const Real err = B(x) - B(x_base) -  h * x_dir * dB_base;
                const Real Norm_Err = fabs(err);

                cout << "Step h:\t"<< h  <<"\t  ,error / h^2: \t" << Norm_Err/(h*h)   << endl;
            }//for nb_h
            cout << "\n "<< endl;
            
        } //for nb_dir
        cout << "\n"<< endl;


    } //for nb_base

    std::cout << "Conclusion ?? " << std::endl;
    std::cout << "Reminder : the relative errors Er / h^2 should be (alsmot) independant of h (if the function and its derivative are well computed)" << std::endl;
    std::cout << "If you see huge variation, go back to your Jacobian notebook :( " << std::endl;

}

//Mean_Fct (mean), implementation 

Mean_Fct::Mean_Fct() //default constructor, arithemtic mean
{
    MeanFctType m;
    MeanFctType dm;
    m =[this](const Real & x, const Real & y) -> Real {
        return 0.5 * (x+y);
    };
    dm =[this](const Real & x, const Real & y) -> Real {
        return 0.5;
    };
    m_mean = m;
    m_d1_mean = dm;
    m_d2_mean = dm;
}


Mean_Fct::Mean_Fct(const std::string & type_mean)
{
    if(type_mean =="ari"){
        MeanFctType m;
        MeanFctType dm;
        m =[this](const Real & x, const Real & y) -> Real {
            return 0.5 * (x+y);
        };
        dm =[this](const Real & x, const Real & y) -> Real {
            return 0.5;
        };
        m_mean = m;
        m_d1_mean = dm;
        m_d2_mean = dm;
    }
    else if(type_mean == "geo"){
        MeanFctType m;
        MeanFctType d1m;
        MeanFctType d2m;
        m =[this](const Real & x, const Real & y) -> Real {
            return sqrt(x*y);
        };
        d1m =[this](const Real & x, const Real & y) -> Real {
            return 0.5*sqrt(y/x);
        };
         d2m =[this](const Real & x, const Real & y) -> Real {
            return 0.5 *sqrt(x/y);
        };
        m_mean = m;
        m_d1_mean = d1m;
        m_d2_mean = d2m;
    }
    else if(type_mean == "log"){
        MeanFctType m;
        MeanFctType d1m;
        MeanFctType d2m;
        m =[this](const Real & x, const Real & y) -> Real {
            const Real h = y-x;
            if(fabs(h)>s_regul){
                return -h/log(x/y);
            }
            else{
                Real h_2 = h*h;
                Real x_inv = 1./x;
                return x + 0.5 * h - (x_inv*h_2)/(12.) + (x_inv*x_inv*h*h_2)/(24.);
            }
        };
        d1m =[this](const Real & x, const Real & y) -> Real {
            const Real h = y-x;           
            if(fabs(h)>s_regul){
                const Real ratio = x/y; 
                const Real l_ = log(ratio);
                return (l_ - 1. + (1./ratio) )/(l_ * l_);
            }
            else{
                const Real r = h/x;
                Real d = 0.5 + r /6.;
                const Real r_2 = r*r;
                d+= r_2/24.;
                d+= (r*r_2)/45.;
                return d;
            }
        };
        d2m =[d1m](const Real & x, const Real & y) -> Real {
            return d1m(y,x);
        };
        m_mean = m;
        m_d1_mean = d1m;
        m_d2_mean = d2m;
    }
    else if(type_mean == "harmo")
    {
        MeanFctType m;
        MeanFctType d1m;
        MeanFctType d2m;
        m =[this](const Real & x, const Real & y) -> Real {
            return (2. * x* y) / (x+y);
        };
        d1m =[this](const Real & x, const Real & y) -> Real {
            const Real quo = 1 - (x /(x+y));
            return 2. * quo*quo;
        };
        d2m =[d1m](const Real & x, const Real & y) -> Real {
            return d1m(y,x);
        };
        m_mean = m;
        m_d1_mean = d1m;
        m_d2_mean = d2m;
    }

    else{
        std::cout <<"You asked for a unknown type of mean function \n"  <<  std::endl;
        std::cout <<"Arithmetic mean will be used instead \n"  <<  std::endl;
        MeanFctType m;
        MeanFctType dm;
        m =[this](const Real & x, const Real & y) -> Real {
            return 0.5 * (x+y);
        };
        dm =[this](const Real & x, const Real & y) -> Real {
            return 0.5;
        };
        m_mean = m;
        m_d1_mean = dm;
        m_d2_mean = dm;
    }   

}

Real Mean_Fct::m(const Real & x, const Real y) const
{
    return m_mean(x,y);
} 

Real Mean_Fct::d1_m(const Real & x, const Real y) const
{
    return m_d1_mean(x,y);
}

Real Mean_Fct::d2_m(const Real & x, const Real y) const
{
    return m_d2_mean(x,y);
}


// Sch_functions, implementation


Sch_functions::Sch_functions()
{
    m_name_B="SG";
    m_name_m="ari";
    m_mean= Mean_Fct(); 
    m_B = B_Fct();
}

Sch_functions::Sch_functions(const std::string & type_B, const std::string & type_m )
{
    m_name_B = type_B;
    m_name_m = type_m;
    m_mean= Mean_Fct(type_m); 
    m_B = B_Fct(type_B);
}

Real Sch_functions::B(const Real & x) const
{
    return m_B.B(x);
}

Real Sch_functions::dB(const Real & x) const
{
    return m_B.dB(x);
}

Real Sch_functions::m(const Real & x, const Real y) const
{
    return m_mean.m(x,y);
}

Real Sch_functions::d1_m(const Real & x, const Real y) const
{
    return m_mean.d1_m(x,y);
}

Real Sch_functions::d2_m(const Real & x, const Real y) const
{
    return m_mean.d2_m(x,y);
}





// Fixed_contrib, implementation   

Fixed_contrib::Fixed_contrib(){}

Fixed_contrib::Fixed_contrib(const Mesh & msh, const int & order,  const PhysicalData & data )
{   
    std::cout << sep << std::endl;
    std::cout << "Precomputation of the fixed contributions: go !" << std::endl;    
    
    stat = data.stat;

    m_mesh = msh;
    m_order_interpolant = order;
    std::vector<Real> transmi;
    for(int iE =0; iE<msh.number_edges(); iE++ ){
        transmi.push_back(1./msh.d_edge(iE));
    } 
    m_transmi = transmi;

    set_discrete_doping(data.f);
    set_discrete_BEE( data.E );    
    set_discrete_mobility( data.mob );

    std::cout << "Precomputation of the fixed contributions:  DONE ! " << std::endl;
    std::cout << sep << std::endl;

}    

void Fixed_contrib::set_discrete_doping(const ReconstructionType & f_ )
{
    m_f = TP_interpolant(f_, m_mesh, m_order_interpolant);
}

void Fixed_contrib::set_discrete_BEE(const ReconstructionType & E_ )
{
    m_Ed = Discrete_u( E_( m_mesh.get_left_boundary_pos() ), E_( m_mesh.get_right_boundary_pos() ), m_mesh );
    m_Ed.set_from_discontinuous_fonction(E_,3); 
    m_omega_partial = -1. * m_Ed;
    m_omega_partial.Apply_function(exp_fct);
}

void Fixed_contrib::set_discrete_mobility(const ReconstructionType & mob_ )
{
    m_mob = TP_interpolant(mob_, m_mesh, m_order_interpolant);
}

Real Fixed_contrib::Tau_s(const int & iE) const
{
    return m_transmi[iE];
}

Real Fixed_contrib::f_K(const int & iK) const
{
    return m_f(iK);
}

Real Fixed_contrib::E_K(const int & iK) const
{
    return m_Ed.cell_value(iK);
}

Real Fixed_contrib::omega_K(const int & iK) const
{
    return m_omega_partial.cell_value(iK);
}


Real Fixed_contrib::mob_K(const int & iK) const
{
    return m_mob(iK);
}

Discrete_u Fixed_contrib::get_discrete_BEE() const
{
    return m_Ed; 
} 

Discrete_u Fixed_contrib::get_discrete_omega() const
{
    return m_omega_partial; 
} 



// Flux, implementation    

Flux::Flux(){}

Flux::Flux(const Mesh & mesh, const Fixed_contrib & contribs, const Sch_functions & flux_fct, const std::string & type_flux)
{ 
    std::cout << "\n Creation of the fluxes \n" << std::endl; 
    m_mesh = mesh; 
    fix_contr = contribs;
    m_flux_fct = flux_fct;
    m_stat = contribs.stat;   
    std::string flx = type_flux;  
    std::cout << "Number of cells: " << m_mesh.number_cells() << std::endl;
    bool FluxisAct = (type_flux == "activity");
    bool FluxisExpF = (type_flux == "expfittact");
    bool FluxisSEDAN = (type_flux == "SEDAN");
    bool FluxisImplemented = FluxisAct || FluxisExpF || FluxisSEDAN;
    if(not(FluxisImplemented)){
        std::cout << "Unknown type of flux \n" << std::endl;
        std::cout << "Activity based fluxes used instead\n" << std::endl;
        flx = "activity";
    }
    m_type_flux = flx;
    m_omega = fix_contr.get_discrete_omega();
    std::cout << "The flux is a "<< m_type_flux << " type " << std::endl;
}

Real Flux::F_Ks(const int & iK, const int & iE) const
{   
    if(m_type_flux == "activity"){
        
        const Real c_K = m_c.cell_value(iK);
        const Real c_L = m_c.Ks_value(iK,iE);

        const Real average_beta = m_flux_fct.m(m_stat.Beta(c_K), m_stat.Beta(c_L) );

        const Real arg = m_B_arg.D_Ks(iK,iE);
        const Real B_K = m_flux_fct.B( arg ) * m_stat.a(c_K) ;
        const Real B_L = m_flux_fct.B(- arg ) * m_stat.a(c_L) ;

        return fix_contr.Tau_s(iE) * average_beta * (B_K - B_L);
    }
    else if(m_type_flux == "expfittact"){
        
        const Real c_K = m_c.cell_value(iK);
        const Real c_L = m_c.Ks_value(iK,iE);

        const Real u_K = m_u_partial.cell_value(iK);  
        const Real u_L = m_u_partial.Ks_value(iK,iE);  

        const Real diffu_K = m_inv_activity_partial.cell_value(iK);  
        const Real diffu_L = m_inv_activity_partial.Ks_value(iK,iE);  
        Real average_diffu = m_flux_fct.m(diffu_K, diffu_L);
        //const Real average_diffu = m_flux_fct.m(m_inv_activity_partial.cell_value(iK), m_inv_activity_partial.Ks_value(iK,iE) );

        const Real arg = m_Phi.D_Ks(iK,iE);
        const Real B_K = m_flux_fct.B( arg ) * u_K ;
        const Real B_L = m_flux_fct.B(- arg ) * u_L ;

        return fix_contr.Tau_s(iE) * average_diffu * (B_K - B_L);
    }    
    else if(m_type_flux == "SEDAN"){
        
        const Real c_K = m_c.cell_value(iK);
        const Real c_L = m_c.Ks_value(iK,iE);

        //const Real average_diffu = m_flux_fct.m(m_inv_activity_partial.cell_value(iK), m_inv_activity_partial.Ks_value(iK,iE) );

        const Real arg = m_B_arg_sedan.D_Ks(iK,iE);
        const Real B_K = m_flux_fct.B( arg ) * c_K ;
        const Real B_L = m_flux_fct.B(- arg ) * c_L ;

        return fix_contr.Tau_s(iE) * (B_K - B_L);
    }
    else{
        std::cout << "Non-specified flux" <<std::endl;
        return 0.;
    }
}
    
Real Flux::dL_F_Ks(const int & iK, const int & iE, const int iL) const
{ 
    if(m_type_flux == "activity"){
        const int iKs = m_mesh.cell_idx_Ks(iK,iE); 
        bool non_zero = (iL == iK) ||(iL == iKs);
        if(not(non_zero)){ return 0.;}     

        const Real Tau_sigma = fix_contr.Tau_s(iE);
        
        const Real c_K = m_c.cell_value(iK);
        const Real c_L = m_c.cell_value(iKs);   
        const Real average_beta = m_flux_fct.m(m_stat.Beta(c_K), m_stat.Beta(c_L) );

        const Real arg = m_B_arg.D_Ks(iK,iE);
        const Real B_K = m_flux_fct.B( arg ) ;
        const Real B_L = m_flux_fct.B(- arg )  ;

        const Real B_KL = B_K * m_stat.a(c_K) - B_L * m_stat.a(c_L); 

        if(iL == iK){
            const Real d_mean = m_flux_fct.d1_m(m_stat.Beta(c_K),m_stat.Beta(c_L)) * m_stat.dBeta(c_K);
            return Tau_sigma  * (d_mean * B_KL + average_beta *  B_K * m_stat.da(c_K)); 
        } // if iL = iK, diagonal entry
        else{
            const Real d_mean = m_flux_fct.d2_m(m_stat.Beta(c_K),m_stat.Beta(c_L)) * m_stat.dBeta(c_L);
            return Tau_sigma* ( d_mean * B_KL - average_beta *  B_L * m_stat.da(c_L)); 
        } // if iL = iKs

    }
    else if(m_type_flux == "expfittact"){        
        
        const int iKs = m_mesh.cell_idx_Ks(iK,iE); 
        bool non_zero = (iL == iK) ||(iL == iKs);
        if(not(non_zero)){ return 0.;} 
        // std::cout << "arg_1" <<std::endl;
        // auto begin = std::chrono::high_resolution_clock::now();
        
        const Real Tau_sigma = fix_contr.Tau_s(iE); 
        
        const Real c_K = m_c.cell_value(iK);
        const Real c_L = m_c.Ks_value(iK,iE);

        // auto end = std::chrono::high_resolution_clock::now();
        // auto elapsed = std::chrono::duration_cast<std::chrono::nanoseconds>(end - begin);
        // std::cout << "c ?? : \t" << c_K << "  " << c_L  << " \t\t " << elapsed.count() <<std::endl;
        // begin = std::chrono::high_resolution_clock::now();
        

        const Real omega_K = m_omega.cell_value(iK);
        const Real omega_L =  m_omega.Ks_value(iK,iE);
        //  end = std::chrono::high_resolution_clock::now();
        // elapsed = std::chrono::duration_cast<std::chrono::nanoseconds>(end - begin);
        // std::cout << "omega ?? : \t" << omega_K << "  " << omega_L << " \t\t " << elapsed.count()   <<  std::endl;
        // begin = std::chrono::high_resolution_clock::now();

        const Real u_K = m_u_partial.cell_value(iK);  
        const Real u_L = m_u_partial.Ks_value(iK,iE);  

        // end = std::chrono::high_resolution_clock::now();
        // elapsed = std::chrono::duration_cast<std::chrono::nanoseconds>(end - begin);
        // std::cout << "u ?? : \t" << u_K << "  " << u_L << " \t\t " << elapsed.count()  <<std::endl;
        // begin = std::chrono::high_resolution_clock::now();


        const Real diffu_K = m_inv_activity_partial.cell_value(iK);  
        const Real diffu_L = m_inv_activity_partial.Ks_value(iK,iE);  

        const Real average_diffu = m_flux_fct.m(diffu_K, diffu_L );

        const Real arg = m_Phi.D_Ks(iK,iE);
        const Real B_K = m_flux_fct.B( arg )* u_K  ;
        const Real B_L = m_flux_fct.B(- arg ) * u_L  ;
        
        const Real B_KL = B_K  - B_L; 
        
        // end = std::chrono::high_resolution_clock::now();
        // elapsed = std::chrono::duration_cast<std::chrono::nanoseconds>(end - begin);
        // std::cout << "arg_2" << " \t\t" << elapsed.count() <<std::endl;
        
        if(iL == iK){
            const Real d_mean = m_flux_fct.d1_m(diffu_K, diffu_L) * m_stat.dBeta(c_K)* omega_K;
            //std::cout << "arg_3.1" <<std::endl;
            return Tau_sigma  * (d_mean * B_KL + average_diffu *  B_K  * m_stat.dh(c_K)); 
            
        } // if iL = iK, diagonal entry
        else{
            const Real d_mean = m_flux_fct.d2_m(diffu_K, diffu_L) * m_stat.dBeta(c_L)* omega_L;
            //std::cout << "arg_3.2" <<std::endl;
            return Tau_sigma* ( d_mean * B_KL - average_diffu *  B_L * m_stat.dh(c_L));             
            
        } // if iL = iKs
    }
    else if(m_type_flux == "SEDAN"){        
        
        const int iKs = m_mesh.cell_idx_Ks(iK,iE); 
        bool non_zero = (iL == iK) ||(iL == iKs);
        if(not(non_zero)){ return 0.;} 
        // std::cout << "arg_1" <<std::endl;
        // auto begin = std::chrono::high_resolution_clock::now();
        
        const Real Tau_sigma = fix_contr.Tau_s(iE); 
        
        const Real c_K = m_c.cell_value(iK);
        const Real c_L = m_c.Ks_value(iK,iE);        

        // auto end = std::chrono::high_resolution_clock::now();
        // auto elapsed = std::chrono::duration_cast<std::chrono::nanoseconds>(end - begin);
        // std::cout << "c ?? : \t" << c_K << "  " << c_L  << " \t\t " << elapsed.count() <<std::endl;
        // begin = std::chrono::high_resolution_clock::now();
                
        const Real expot_K = m_expot.cell_value(iK);
        const Real expot_L = m_expot.Ks_value(iK,iE);

        // end = std::chrono::high_resolution_clock::now();
        // elapsed = std::chrono::duration_cast<std::chrono::nanoseconds>(end - begin);
        // std::cout << "expot ?? : \t" << expot_K << "  " << expot_L << " \t\t " << elapsed.count()   <<  std::endl;
        // begin = std::chrono::high_resolution_clock::now();

        const Real arg = m_B_arg_sedan.D_Ks(iK,iE);        
        
        // end = std::chrono::high_resolution_clock::now();
        // elapsed = std::chrono::duration_cast<std::chrono::nanoseconds>(end - begin);
        // std::cout << "arg ?? : \t" << arg << " \t\t " << elapsed.count()  <<std::endl;
        // begin = std::chrono::high_resolution_clock::now();
        const Real B_K = m_flux_fct.B( arg );
        
        const Real dB_K = m_flux_fct.dB( arg ) * c_K  ;
        const Real dB_L = m_flux_fct.dB(- arg ) *c_L  ;
        
        const Real dB_KL = dB_K  + dB_L; 
        
        // end = std::chrono::high_resolution_clock::now();
        // elapsed = std::chrono::duration_cast<std::chrono::nanoseconds>(end - begin);
        // std::cout << "eval B" << " \t\t" << elapsed.count() <<std::endl;
        
        if(iL == iK){
            //std::cout << "arg_3.1" <<std::endl;
            const Real B_K = m_flux_fct.B( arg );
            return Tau_sigma  * (B_K - m_stat.dexpot(c_K) * dB_KL ); 
            
        } // if iL = iK, diagonal entry
        else{        
            const Real B_L = m_flux_fct.B(- arg ) ;        

            //std::cout << "arg_3.2" <<std::endl;
            return Tau_sigma* ( -B_L + m_stat.dexpot(c_L) * dB_KL);             
            
        } // if iL = iKs 
    }
    else{
        std::cout << "Non-specified flux" <<std::endl;
        return 0.;
    }
   
}

void Flux::set_concentration(const Discrete_u & c)
{
    m_c = c;
    //u_K = exp( m_stat.h(c_K) + E_K)
    m_u_partial = c;
    m_u_partial.Apply_function( m_stat.h_fct());
    m_u_partial += fix_contr.get_discrete_BEE();
    m_u_partial.Apply_function(exp_fct);

    m_inv_activity_partial= m_c / m_u_partial; 
            
    m_expot = c;
    m_expot.Apply_function(m_stat.expot_fct());
}

void Flux::set_potential(const Discrete_u & Phi)
{
    m_Phi = Phi;
    m_B_arg = fix_contr.get_discrete_BEE() + m_Phi;
    m_B_arg_sedan = m_B_arg + m_expot;
}

void Flux::print_nb_cells() const
{
    std::cout << "Number of cells (according to flux): " << m_mesh.number_cells() << std::endl;
} 

/*
// Activity_Flux, implementation 

Activity_Flux::Activity_Flux(const Mesh & mesh, const Fixed_contrib & contribs, const Sch_functions & flux_fct)
{ 
    std::cout << "\n Creation of the fluxes \n" << std::endl; 
    m_mesh = mesh; 
    fix_contr = contribs;
    m_flux_fct = flux_fct;
    m_stat = contribs.stat;     
    std::cout << "Number of cells: " << m_mesh.number_cells() << std::endl;

}


Real Activity_Flux::F_Ks(const int & iK, const int & iE) const
{       

    m_c.print_values_debug();
    
    const Real c_K = m_c.cell_value(iK);
    const Real c_L = m_c.Ks_value(iK,iE);

    const Real average_beta = m_flux_fct.m(m_stat.Beta(c_K), m_stat.Beta(c_L) );

    const Real arg = m_B_arg.D_Ks(iK,iE);
    const Real B_K = m_flux_fct.B( arg ) * c_K ;
    const Real B_L = m_flux_fct.B(- arg ) * c_L ;

    return fix_contr.Tau_s(iE) * average_beta * (B_K - B_L);
}


Real Activity_Flux::dL_F_Ks(const int & iK, const int & iE, const int iL) const
{
    const int iKs = m_mesh.cell_idx_Ks(iK,iE); 
    bool non_zero = (iL == iK) ||(iL == iKs);
    if(not(non_zero)){ return 0.;}     
    
    const Real c_K = m_c.cell_value(iK);
    const Real c_L = m_c.cell_value(iKs);   
    const Real average_beta = m_flux_fct.m(m_stat.Beta(c_K), m_stat.Beta(c_L) );

    const Real arg = m_B_arg.D_Ks(iK,iE);
    const Real B_K = m_flux_fct.B( arg ) ;
    const Real B_L = m_flux_fct.B(- arg )  ;

    const Real B_KL = B_K * c_K - B_L * c_L; 

    if(iL == iK){
        const Real d_mean = m_flux_fct.d1_m(m_stat.Beta(c_K),m_stat.Beta(c_L)) * m_stat.dBeta(c_K);
        return d_mean * B_KL + average_beta *  B_K; 
    } // if iL = iK, diagonal entry
    else{
        const Real d_mean = m_flux_fct.d2_m(m_stat.Beta(c_K),m_stat.Beta(c_L)) * m_stat.dBeta(c_L);
        return d_mean * B_KL - average_beta *  B_L; 
    } // if iL = iKs
}
*/


//Newton method, implementation

Newton_method::Newton_method()
{ }
    


Newton_method::Newton_method( const Mesh & mesh,const Fixed_contrib & contribs ,  Flux & flux)
{   
    m_flux = flux;    
    m_print_info = true;
    m_max_it = max_iter;
    m_stopping_crit = goal_residue_size;
    m_fix_contr = contribs;
    m_mesh = mesh; 
    m_nb_unkn= m_mesh.number_cells(); 

}


Newton_method::Newton_method( const Mesh & mesh,const Fixed_contrib & contribs ,  Flux & flux,
                 const bool & complete_infos, const int & max_iterations, const Real & stopping_crit)
{
    m_flux = flux;
    m_print_info = complete_infos;
    m_max_it = max_iterations;
    m_stopping_crit = stopping_crit;    

    m_fix_contr = contribs;
    m_mesh = mesh; 
    m_nb_unkn= m_mesh.number_cells();
}

Newton_method::~Newton_method()
{
    //delete m_flux;
}

void Newton_method::set_eq_boundary_values(const Real & c_eq_left, const Real & c_eq_right )
{
    m_c_eq_left = c_eq_left;
    m_c_eq_right = c_eq_right;
}

void Newton_method::set_goal_boundary_values(const Real & c_goal_left, const Real & c_goal_right )
{
    m_c_goal_left = c_goal_left;
    m_c_goal_right = c_goal_right;

}

void Newton_method::set_continuation_parameter(const Real & s)
{
    m_s = s; 
    m_c_lb = s * m_c_goal_left + (1. - s ) * m_c_eq_left;
    m_c_rb = s * m_c_goal_right + (1. - s ) * m_c_eq_right;
}

void Newton_method::initialize_method(const Discrete_u & c_ini,const Discrete_u & Phi_ini )
{
    m_c = c_ini;
    m_c.Proj_on_Ih_eps(m_fix_contr.stat, s_proj);

    m_res = 0. * c_ini;

    m_Phi = Phi_ini; 

    m_can_continue = true; 
    m_is_CV = false;
    m_nb_it = 0; 
}

void Newton_method::enforce_boundary_values()
{
    m_c.set_boundary_values(m_c_lb,m_c_rb);
    m_res.set_boundary_values(0.,0.);
}
 
void Newton_method::actualize_flux()
{
    m_flux.set_concentration(m_c);
    m_flux.set_potential(m_Phi);// not needed here, since Phi is a given function ... BUT the actualisation of B_arg lies in this function so ...
}

void Newton_method::compute_goal_function_G() 
{       
    //std::cout << "Computation fluxes \n"<< std::endl;
    m_G = SolutionVectorType::Zero(m_nb_unkn);
    for(int iK =0; iK < m_nb_unkn; iK++){
        const Cell K = m_mesh.cell(iK);
        m_G(iK) += - m_s * K.size() * m_fix_contr.f_K(iK);
        
        const NeighIdArrayType edges_Id= K.edges_id();
        for( int iE_loc = 0; iE_loc < 2; iE_loc++ ){
            const int iE = edges_Id[iE_loc];
            m_G(iK) += m_flux.F_Ks(iK,iE); 
            //std::cout << "F_Ks ("<< iK << ","<< iE <<") : \t " << m_flux.F_Ks(iK,iE)<< std::endl;
        } //for iE_loc
    } // for iK 
}

void Newton_method::get_bounds_conformity()
{
    bool I_h_valued = true;
    int iK = 0;
    while( I_h_valued && (iK < m_nb_unkn)){
        I_h_valued = m_fix_contr.stat.is_Ih_valued(m_c.cell_value(iK));
        iK+=1;
    }
    m_bounds =  I_h_valued;
}

void Newton_method::compute_goal_norm()
{
    m_norm_goal = m_G.lpNorm<Eigen::Infinity>();
    m_goal_is_small = (m_norm_goal<m_stopping_crit); 
}

void Newton_method::compute_relative_res_norm()
{
    m_norm_rel_residue = m_res.L_infty_norm()/m_c.L_infty_norm();  //(m_res/m_c).L_infty_norm();
    m_res_is_small = (m_norm_rel_residue < m_stopping_crit);
    m_res_is_very_small = (m_norm_rel_residue < 0.01 * m_stopping_crit);
}

void Newton_method::launch_iteration()
{    
    std::cout << "\niteration begins"<< std::endl;
    enforce_boundary_values();
    actualize_flux();
    compute_goal_function_G();
    std::cout << "test 2 "<< std::endl;
    ListTripletType Triplets;
    Triplets.reserve(4 * m_nb_unkn);

    for(int iK =0; iK < m_nb_unkn; iK++){
        const Cell K = m_mesh.cell(iK);
        
        const NeighIdArrayType edges_Id= K.edges_id();
        for( int iE_loc = 0; iE_loc < 2; iE_loc++ ){
            const int iE = edges_Id[iE_loc];
            Triplets.push_back(TripletType(iK,iK,m_flux.dL_F_Ks(iK,iE,iK)));
            
            if( not(m_mesh.idx_Ks_isboundary(iK,iE))){
                const int iKs = m_mesh.cell_idx_Ks(iK,iE);
                Triplets.push_back(TripletType(iK,iKs,m_flux.dL_F_Ks(iK,iE,iKs)));
            }
            
        } //for iE_loc
    } // for iK 
    //std::cout << m_G << std::endl;
    std::cout << " ~~ Triplets computed"<< std::endl;
    m_res.set_from_triplets(Triplets, - m_G);
    m_c += m_res; 
    m_nb_it+=1;

    compute_goal_norm();
    compute_relative_res_norm();
    get_bounds_conformity();
}

void Newton_method::print_infos_iteration() const
{
    std::cout << "Information about the iteration number \t"<< m_nb_it << std::endl;
    std::cout << "Does the Newton method converge  ? \t"<< m_is_CV << std::endl;
    std::cout << "Is the discrete density computed I_h-valued  ? \t"<< m_bounds << std::endl;
    std::cout << "Norm of the goal function: \t"<< m_norm_goal << std::endl;
    std::cout << "Relative norm of the residue: \t"<< m_norm_rel_residue << std::endl;
}

bool Newton_method::launch_method() //return true if the method converges, false if the method did not ...
{   
    // launch iteration 
    
    do{ //boucle Newton

        launch_iteration();   
       
        bool too_much_iter=(m_nb_it > m_max_it);
        bool CV_indicators_are_small = m_res_is_very_small || (m_res_is_small && m_goal_is_small);
    
        m_is_CV = (!too_much_iter) && CV_indicators_are_small && m_bounds;
        m_can_continue = m_bounds && (! CV_indicators_are_small) && (! too_much_iter);

        if(m_print_info){ print_infos_iteration();}
       
    }while(m_can_continue) ; //boucle Newton

    return m_is_CV;
}

Discrete_u Newton_method::get_c() const
{
    return m_c; 
}

int Newton_method::get_number_iteration() const
{
    return m_nb_it;
}


void Newton_method::Gaudeul_test(const int & number_base_points, const int & number_dir )
{   
    std::cout << sep << std::endl;
    std::cout << "Performing a test of the Jacobian a la Gaudeul "<< std::endl;
    std::cout << "May it be a success ... " << std::endl;
    const Real epsilon = 0.0000001;

    set_eq_boundary_values(0.5, 1.);
    set_goal_boundary_values(1.,1.);
    set_continuation_parameter(1.);

    cout << "New base point. "<< endl;
    for(int nb_base = 0; nb_base < number_base_points; nb_base ++){
        cout << "Base point number "<< nb_base +1 << endl;

        Discrete_u c_base = Discrete_u(m_c_lb,m_c_rb,m_mesh); 
        //std::cout << " test 1.0" << std::endl;
        const SolutionVectorType vec_base = SolutionVectorType::Random(m_nb_unkn);
        //std::cout << " test 1.1" << std::endl;
        c_base.set_from_values(vec_base);
        c_base.Proj_on_Ih_eps(m_fix_contr.stat, epsilon);
        
        m_c = c_base;
        

        enforce_boundary_values();
        actualize_flux();
        compute_goal_function_G();
        

        ListTripletType Triplets;
        Triplets.reserve(4 * m_nb_unkn);

        for(int iK =0; iK < m_nb_unkn; iK++){
            const Cell K = m_mesh.cell(iK);
            
            const NeighIdArrayType edges_Id= K.edges_id();
            for( int iE_loc = 0; iE_loc < 2; iE_loc++ ){
                const int iE = edges_Id[iE_loc];
                Triplets.push_back(TripletType(iK,iK,m_flux.dL_F_Ks(iK,iE,iK)));
                
                            
                if( not(m_mesh.idx_Ks_isboundary(iK,iE))){
                    const int iKs = m_mesh.cell_idx_Ks(iK,iE);
                    Triplets.push_back(TripletType(iK,iKs,m_flux.dL_F_Ks(iK,iE,iKs)));
                }
            } //for iE_loc
        } // for iK 

        SparseMatrixType Jac(m_nb_unkn, m_nb_unkn);
        Jac.setFromTriplets(Triplets.begin(), Triplets.end());
        

        const SolutionVectorType G_base = m_G; 
        cout << "New direction !"<< endl;
        for(int nb_dir = 0; nb_dir <number_dir; nb_dir++){
            cout << "Direction number "<< nb_dir +1 << endl;
            cout << "   " << endl;

            Discrete_u c_dir = Discrete_u(m_c_lb,m_c_rb,m_mesh);
            SolutionVectorType vec_dir = SolutionVectorType::Random(m_nb_unkn).cwiseAbs();
            c_dir.set_from_values(vec_dir);
            
            const SolutionVectorType J_dir = Jac * vec_dir;

            for(int nb_h = 1; nb_h < 8; nb_h ++){
                Real h = 1. * std::pow(10.,  -nb_h);
             
                    
                m_c  = c_base + h * c_dir;
                
                enforce_boundary_values();
                actualize_flux();
                compute_goal_function_G();

                const SolutionVectorType err = m_G - G_base - h *J_dir;
                const Real Norm_Err = err.lpNorm<Eigen::Infinity>();

                cout << "Step h  :"<< h  <<", error / h^2  :" << Norm_Err/(h*h)   << endl;
                cout << "Error norm : " << Norm_Err   << endl;
            }//for nb_h
            cout << "\n "<< endl;
            
        } //for nb_dir
        cout << "\n"<< endl;


    } //for nb_base

    std::cout << "Conclusion ?? " << std::endl;
    std::cout << "Reminder : the relative errors Er / h^2 should be (alsmot) independant of h (if the Jacobian and G are well computed)" << std::endl;
    std::cout << "If you see huge variation, go back to your Jacobian notebook :( " << std::endl;
    std::cout << "Remark : if the scheme under study is linear, just take a look at the values of the error vector ... It should be at the machine error " << std::endl;
    std::cout << "\n" << sep << "\n"<< std::endl;


}


/*
    private:
    Flux *m_flux;
    Mesh m_mesh;
    int m_nb_unkn;
    
    Discrete_u m_Phi;
    
    Discrete_u m_c;
    Discrete_u m_res;
    Discrete_u m_G; 
     
    Real m_c_eq_left;
    Real m_c_eq_right;
    
    Real m_s;

    bool m_is_CV;
    bool m_can_continu;
    bool m_goal_is_small;
    bool m_res_is_small;
    bool m_bounds;

    int m_max_it;
    Real m_stopping_crit;
    bool m_print_info;
*/
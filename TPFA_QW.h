// #include "Model_data.h"
#include "Scheme.h"



#include "eigen3/Eigen/SparseLU"
#include "eigen3/Eigen/Core"

#include <eigen3/unsupported/Eigen/SparseExtra>

#include <boost/integer/static_min_max.hpp>
#include <boost/math/constants/constants.hpp>
#include <boost/numeric/ublas/vector.hpp>

#include <vector>

#include <iostream>
#include <eigen3/Eigen/Dense>

using namespace std;

#include <fstream>
#include <string>
#include <stdio.h>
#include <string.h>


#ifndef sep
#define sep  "\n----------------------------------------\n----------------------------------------\n\n"
#endif

#ifndef ds_crit
#define ds_crit 1.*std::pow(10., -30)
#endif

#ifndef tol_stopping
#define tol_stopping 0.5*std::pow(10., -13)
#endif


Discrete_u sch_sta_classic(const Mesh & msh, const PhysicalData & data, const Sch_functions & sch_fct, const std::string & flux_type)
{   

    std::cout << sep << "Stationary scheme for the advection-diffusion equation" << std::endl;
    ////////////////////
    ////////////////////
    //Discrete data  
    ////////////////////
    ////////////////////


    RealFctType g_fct=[data] (const Real & s) -> Real {
        return data.stat.g(s);
    };
    std::cout << "Step 5" << std::endl;
    Discrete_u Phi = Discrete_u(data.Phi,msh);     
    std::cout << "Step 6" << std::endl;

    Phi.set_from_fonction(data.Phi,3);
    std::cout << "Step 7" << std::endl;

    const Fixed_contrib fc = Fixed_contrib(msh,3,data);

    Discrete_u E = fc.get_discrete_BEE();

    //Discrete_u E(  E_ , mesh);    
    //E.set_from_fonction(E_, 3);


    const Point x_lb = msh.get_left_boundary_pos();
    const Point x_rb = msh.get_right_boundary_pos();
    const Real c_eq_left = data.stat.g( -data.Phi(x_lb) - data.E(x_lb));
    const Real c_eq_right = data.stat.g( -data.Phi(x_rb) - data.E(x_rb));
    Discrete_u c_eq = (-1.*(Phi + E));
    c_eq.Apply_function(g_fct);

    std::cout << " Equilibrium " << std::endl;
    //c_eq.print_values_debug(); 


    ////////////////////
    ////////////////////
    //Newton's method 
    ////////////////////
    ////////////////////
    
    Flux flux(msh,fc,sch_fct, flux_type);

    
    //Creation of the Newton method
    Newton_method NM = Newton_method(msh, fc,  flux);

    //Setting the boundary values (goal and equilibrium)
    NM.set_eq_boundary_values( c_eq_left , c_eq_right );
    NM.set_goal_boundary_values(data.c_b(x_lb),data.c_b(x_rb) );
    std::cout << " bound_goal values " << data.c_b(x_lb) << "  " <<data.c_b(x_rb)  << std::endl;
    std::cout << " pos boundaries " << x_lb << "  " << x_rb  << std::endl;
    NM.initialize_method(c_eq,Phi); // method initialised with the equilibrium 
            
    Real s =0.; // continuation parameter
    Real ds = 1.;
    std::vector<Real> List_s;
    List_s.push_back(s);

    bool Method_converges = false; 
    Discrete_u c_ini = c_eq;

    std::vector<int> Nb_iter_Newton;
    std::vector<int> Nb_resol_cumul;
    Nb_iter_Newton.push_back(0);
    Nb_resol_cumul.push_back(0);
    int n_methods = 0;
    int n_resol = 0;    

    while( (s < 1. - tol_stopping ) && (ds > ds_crit )){

        // choice of the jumps in the continuation parameter
        Real max_step = std::min(1. - s, 1.);
        ds = std::min(ds, max_step);

        n_methods += 1;
           
        do{ //boucle newton  adapatative continuation strategy        
            
            NM.initialize_method(c_ini,Phi);
            NM.set_continuation_parameter(s + ds);
            
            std::cout << "Newton method number "<< n_methods <<" with ds = "<< ds << ": go! " << std::endl;
            Method_converges = NM.launch_method(); 

            n_resol += NM.get_number_iteration();

            if(Method_converges){
                s += ds;
                List_s.push_back(s);
                std::cout << "Computation of the solution with continuation parameter s = \t" << s << " :  \t OK"<< std::endl;
                ds = ds * (1. + 0.8);

                Nb_iter_Newton.push_back(NM.get_number_iteration());
                Nb_resol_cumul.push_back(n_resol);
                c_ini = NM.get_c();

            } //if Newton CV
            else{
                std::cout << "Computation of the solution with continuation parameter s = \t" << s + ds << " :  \t Failure"<< std::endl;
                std::cout << "Reduction of the continuation parameter step"<< std::endl;
                ds = 0.5 * ds;
            }

        }while(!Method_converges && (ds > ds_crit )); // boucle newton avec pas temps adaptatif

    } //boucle on s 

    if( (ds< ds_crit) && (s < 1.- tol_stopping ) ){
        std::cout << " \n \n "<< std::endl;
        std::cout << " !!!!!!!!!!!!!!!!!!! !!!!!!!!!!!!!!!!!!!  !!!!!!!!!!!!!!!!!!! " << std::endl;
        std::cout << "Current continuation parameter step : \n" << ds << std::endl;
        std::cout << "Resolution failure ! " << std::endl;
        return Discrete_u(msh);
    } // if effective continuation parameter step becomes too small

    std::cout << "Computation of the stationnary solution : DONE !" << std::endl;

    //Discrete_u c_sol = Discrete_u(NM.get_c());
    Discrete_u c_activity = NM.get_c();
    //c_sol.print_values_debug();

    
    return c_activity;
}


Discrete_u  sch_sta_reg(const Mesh & dualmsh, const PhysicalData & data, const Sch_functions & sch_fct, const std::string & flux_type )
{    
    std::cout << "WIAS style regularisation used !" << std::endl;
    //creation of a new set of physical data with regularized BEE
    PhysicalData data_reg = data;
    //const BandEdgeType E_WIAS = get_E_WIAS_style(data.E,dualmsh,3);
    //data_reg.E = E_WIAS;

    Discrete_u c_sol = sch_sta_classic(dualmsh, data_reg, sch_fct, flux_type);
    return c_sol;

}


int Convergence_test_with_reg(const Geo & geo, const PhysicalData & data,  const Sch_functions & sch_fct, const std::string & flux_type, const Real & h_ini, const int & nb_step, const std::string & name_id)
{   
    std::cout <<  sep << endl;
    std::cout << "Proceding to a convergence analysis, on both primal and dual mesh" << endl;
    std::cout << "Please be sure that the physical data are relevant (cf Model_data.h) " << endl; 

    std::vector<Real> Err_l2_cl; 
    std::vector<Real> Err_h1_cl;

    std::vector<Real> Err_l2_reg;    
    std::vector<Real> Err_false_l2_reg; 
    std::vector<Real> Err_h1_reg;

    std::vector<Real> Mesh_size;
    std::vector<Real> Dual_mesh_size;
    
    for(int n = 0; n< nb_step; n++){
        const Real h = h_ini * std::pow(3.,-n);
        
        const Mesh mesh(h,geo); 
        const Mesh dual_msh = get_dual_mesh(mesh);
        Mesh_size.push_back(mesh.size());
        Dual_mesh_size.push_back(dual_msh.size());    
        
        //Physical data
        BandEdgeType E_ = data.E;
        Discrete_u E(  E_ , mesh);   
        E.set_from_fonction(E_, 3);
    
        BandEdgeType E_WIAS = get_E_WIAS_style(E_,dual_msh,3);    
        
        Discrete_u c_cl=Discrete_u(mesh);
        c_cl = sch_sta_classic(mesh, data, sch_fct, flux_type);
        Discrete_u c_sol = Discrete_u(data.c_exact,mesh);
        c_sol.set_from_fonction(data.c_exact,3);
        Discrete_u err = c_sol - c_cl; 
        

        //err.print_values_debug();

        const Real norm_l2_sol_cl = c_sol.L2_square_norm();
        const Real norm_h1_sol_cl = c_sol.H1_square_norm(); // Rmk : the solution (in c) is not in H1, hence the norm of the solution tends to infinity ...
        std::cout << "H1 norm of the sol ?? \t" << norm_h1_sol_cl << endl;


        Err_h1_cl.push_back(sqrt( err.H1_square_norm() ));
        Err_l2_cl.push_back(sqrt( err.L2_square_norm() / norm_l2_sol_cl));    

        err/=c_sol;    
        err.file_visu_recons("CV/" +name_id+ "/error_profil_cl.txt",0,300);

    
        Discrete_u c_reg=Discrete_u(dual_msh);
        c_reg = sch_sta_reg(dual_msh, data, sch_fct, flux_type); 
        Discrete_u c_sol_reg = Discrete_u(data.c_exact,dual_msh);
        c_sol_reg.set_from_discontinuous_fonction(data.c_exact,3);
        Discrete_u err_reg = c_sol_reg - c_reg;         


        const Real norm_l2_sol_reg = c_sol_reg.L2_square_norm();
        const Real norm_false_l2_sol_reg = c_sol_reg.false_L2_square_norm();
        const Real norm_h1_sol_reg = c_sol_reg.H1_square_norm(); 

        Err_h1_reg.push_back(sqrt( err_reg.H1_square_norm() ));
        Err_l2_reg.push_back(sqrt( err_reg.L2_square_norm() / norm_l2_sol_reg));
        Err_false_l2_reg.push_back(sqrt( err_reg.false_L2_square_norm() / norm_false_l2_sol_reg));        
        
        err_reg /=c_sol_reg;
        err_reg.file_visu_recons("CV/" +name_id+ "/error_profil_reg.txt",0,300);


    }

    // generating files for visualisation of energy band and solution 
    const Mesh mesh_visu(0.01,geo);      
    const Mesh dual_msh_visu = get_dual_mesh(mesh_visu);  

    BandEdgeType E_ = data.E;
    Discrete_u E(  E_ , mesh_visu);   
    E.set_from_fonction(E_, 3); 
    E.file_visu_recons("CV/energyband", 0, 300);
    
    BandEdgeType E_WIAS = get_E_WIAS_style(E_,dual_msh_visu,3);
    Discrete_u E_reg(  E_WIAS , dual_msh_visu);
    E_reg.set_from_fonction(E_WIAS, 3);
    E_reg.file_visu_recons("CV/energyband_WIAS", 0, 300);        
    
    Discrete_u c_cl_visu=Discrete_u(mesh_visu);
    c_cl_visu = sch_sta_classic(mesh_visu, data, sch_fct, flux_type);
    c_cl_visu.file_visu_cst("CV/" +name_id+ "/ex_sol_cl.txt");        
    Discrete_u c_sol_visu = Discrete_u(data.c_exact,mesh_visu);
    c_sol_visu.set_from_fonction(data.c_exact,3); 
    c_sol_visu.file_visu_cst("CV/" +name_id+ "/ex_sol_exact_cl.txt");

    cout << "value sol left:\t" << data.c_exact(0.) << endl;     
    cout << "value sol right:\t" << data.c_exact(1.) << endl;  
    cout << "value sol right - eps:\t" << data.c_exact(1.-0.00000000001) << endl; 
    //c_sol_visu.print_values_debug();


    Discrete_u c_reg_visu=Discrete_u(dual_msh_visu);
    c_reg_visu = sch_sta_reg(dual_msh_visu, data, sch_fct, flux_type);
    c_reg_visu.file_visu_cst("CV/" +name_id+ "/ex_sol_reg.txt");
    Discrete_u c_sol_reg_visu = Discrete_u(data.c_exact,dual_msh_visu);
    c_sol_reg_visu.set_from_discontinuous_fonction(data.c_exact,3); 
    c_sol_reg_visu.file_visu_cst("CV/" +name_id+ "/ex_sol_exact_reg.txt");
    //c_reg_visu.print_values_debug();
    //c_sol_reg_visu.print_values_debug();


    //   about the convergence  
    std::ofstream file_graph;
    file_graph.open ("CV/" +name_id+ "/errors_graph.txt");
    file_graph << "mesh_size" << " " <<  "rel_er_l2_cl" << " " << "rel_er_h1_cl" << " "<< "dual_mesh_size" << " " << "rel_er_l2_reg" << " " << "rel_er_h1_reg" << " " << "rel_er_false_l2_reg" <<std::endl;
    for(int n = 0; n < nb_step; n  ++){
        file_graph << Mesh_size[n] << " " <<  Err_l2_cl[n] << " " << Err_h1_cl[n] << " "<< Dual_mesh_size[n] << " " << Err_l2_reg[n] << " " << Err_h1_reg[n]<< " " << Err_false_l2_reg[n] << endl;
    }//for sch
      file_graph.close();
    
}



int Convergence_test(const Geo & geo, const PhysicalData & data,  const Sch_functions & sch_fct, const std::string & flux_type, const Real & h_ini, const int & nb_step, const std::string & name_id)
{   
    std::cout <<  sep << endl;
    std::cout << "Proceding to a convergence analysis" << endl;
    std::cout << "Please be sure that the physical data are relevant (cf Model_data.h) " << endl; 

    std::vector<Real> Err_l2_cl; 
    std::vector<Real> Err_h1_cl;


    std::vector<Real> Mesh_size;
    
    for(int n = 0; n< nb_step; n++){
        const Real h = h_ini * std::pow(3.,-n);
        
        const Mesh mesh(h,geo); 
        Mesh_size.push_back(mesh.size());
        
        //Physical data
        BandEdgeType E_ = data.E;
        Discrete_u E(  E_ , mesh);   
        E.set_from_fonction(E_, 3);
        
        Discrete_u c_cl=Discrete_u(mesh);
        c_cl = sch_sta_classic(mesh, data, sch_fct, flux_type);
        Discrete_u c_sol = Discrete_u(data.c_exact,mesh);
        c_sol.set_from_fonction(data.c_exact,3);
        Discrete_u err = c_sol - c_cl; 
        

        //err.print_values_debug();

        const Real norm_l2_sol_cl = c_sol.L2_square_norm();
        const Real norm_h1_sol_cl = c_sol.H1_square_norm(); // Rmk : the solution (in c) is not in H1, hence the norm of the solution tends to infinity ...
        //std::cout << "H1 norm of the sol ?? \t" << norm_h1_sol_cl << endl;


        Err_h1_cl.push_back(sqrt( err.H1_square_norm() ));
        Err_l2_cl.push_back(sqrt( err.L2_square_norm() / norm_l2_sol_cl));    

        err/=c_sol;    
        err.file_visu_recons("CV/" +name_id+ "/error_profil_cl.txt",0,300);
    }

    // generating files for visualisation of energy band and solution 
    const Mesh mesh_visu(0.01,geo);      
    const Mesh dual_msh_visu = get_dual_mesh(mesh_visu);  

    BandEdgeType E_ = data.E;
    Discrete_u E(  E_ , mesh_visu);   
    E.set_from_fonction(E_, 3); 
    E.file_visu_recons("CV/energyband", 0, 300);

    Discrete_u c_cl_visu=Discrete_u(mesh_visu);
    c_cl_visu = sch_sta_classic(mesh_visu, data, sch_fct, flux_type);
    c_cl_visu.file_visu_cst("CV/" +name_id+ "/ex_sol_cl.txt");        
    Discrete_u c_sol_visu = Discrete_u(data.c_exact,mesh_visu);
    c_sol_visu.set_from_fonction(data.c_exact,3); 
    c_sol_visu.file_visu_cst("CV/" +name_id+ "/ex_sol_exact_cl.txt");




    //   about the convergence  
    std::ofstream file_graph;
    file_graph.open ("CV/" +name_id+ "/errors_graph.txt");
    file_graph << "mesh_size" << " " <<  "rel_er_l2_cl" << " " << "rel_er_h1_cl" << std::endl;
    for(int n = 0; n < nb_step; n  ++){
        file_graph << Mesh_size[n] << " " <<  Err_l2_cl[n] << " " << Err_h1_cl[n]  << endl;
    }//for sch
      file_graph.close();
    
}
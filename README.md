# tpfa_qw

## README

### Description

The code tpfa_qw was developped to investigate different numerical strategies to approximate convection-diffusion models with "distributional-drift". Such problems were motivated by the study of [random alloy fluctuations in semiconductor devices](https://pubs.aip.org/aip/jap/article/129/7/073104/287240/Multiscale-simulations-of-the-electronic-structure).


This code is designed to approximate the solution of a simplified stationnary model

$$\text{div} \left( u \nabla ( h(u) + \phi + E) \right) = f, $$
where 
* $h$ is a nonlinearity (the so called statistics); 
* $\phi$ is a (smooth) electrostatic potential; 
* $E$ is an irregular potential; called band-edge energy; 
* $f$ is a source term.

The scheme used in based on the Two Point Flux Approximation (TPFA) scheme, and is currently implemented for 1D geometries.
One of its main specificities lies in the fine handling of the meshes and the discontinuities, since the band-edge energy $\phi$ is a piecewise function.

#### Installation
 
The following softwares/libraries are required for compilation:

* C++
* CMake
* Eigen
* Boost 

To compile the code, you should create a build directory.

mkdir build                 <br>
cd build                    <br>
cmake ..                    <br>
make

Once compiled, you can execute "quanw".
In order to get the data (visualisation files and convergence), you should create "CV" and "visu" directories.

mkdir CV                    <br>
mkdir visu                  <br>
./quanw                     <br>


### Getting started

Information about the geometry of the problem (number of jumps of the band edge) and the mesh can be modified in the main function (file TPFA_QW).
You can specify some information about the model (statistics, boundary conditions, electrostatic potential, profiles of the band edge energies, etc) in the PhysicalData constructor (file Model_data.cpp).


### Authors 

Author: Julien Moatti (julien.moatti@tuwien.ac.at)

### License

GNU General Public License v3.0

### Project status

This has been created for personnal use.
There might or might not be updates in the future.


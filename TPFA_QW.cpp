#include "TPFA_QW.h"
//#include "Model_data.h"
//#include "DiscreteData.h"
//#include "Scheme.h"
// #include <Common/GetPot>

#include <boost/integer/static_min_max.hpp>
#include <boost/math/constants/constants.hpp>
#include <boost/numeric/ublas/vector.hpp>


#include "eigen3/Eigen/SparseCore"
#include "eigen3/Eigen/SparseLU"
#include "eigen3/Eigen/Core"

#include <eigen3/unsupported/Eigen/SparseExtra>

#include <vector>

#include <iostream>
#include <fstream>
#include <string>
#include <string.h>

#ifndef sep
#define sep  "\n----------------------------------------\n----------------------------------------\n\n"
#endif

#ifndef ds_crit
#define ds_crit 1.*std::pow(10., -30)
#endif

#ifndef tol_stopping
#define tol_stopping 0.5*std::pow(10., -15)
#endif

typedef std::shared_ptr<Flux> FluxType;


//Some declaration

Discrete_u sch_sta_classic(const Mesh & msh, const PhysicalData & data, const Sch_functions & sch_fct, const std::string & flux_type); 
Discrete_u sch_sta_reg(const Mesh & dualmsh, const PhysicalData & data, const Sch_functions & sch_fct, const std::string & flux_type); 
int Convergence_test_with_reg(const Geo & geo, const PhysicalData & data, const Sch_functions & sch_fct, const std::string & flux_type,  const Real & h_ini, const int & nb_step, const std::string & name_id);
int Convergence_test(const Geo & geo, const PhysicalData & data, const Sch_functions & sch_fct, const std::string & flux_type,  const Real & h_ini, const int & nb_step, const std::string & name_id);


int debug();
int one_mesh();


int main()
{   
    //
    //geometry
    //

    PointsArrayType jumps;   
    const Point x_l = 0.;
    const Point x_r = 1.; 

    const Real size_extremal_slice = 0.4; 
    const int nb_jumps = 4; 
    const Real ratio_pert = 0.1;

    Point x0 = x_l +size_extremal_slice ;
    Point x1 = x_r -size_extremal_slice ;
    
    const Real h_j = (x1-x0)/(nb_jumps+1.);
    std::srand(std::time(nullptr));
    for(int j = 1; j < nb_jumps+1; j++){
        const Real pert = ratio_pert * h_j * ( (std::rand() % 11) - 5.);
        jumps.push_back(x0 + j * h_j + pert);
    }
    // jumps.resize(12);
    // jumps[0] = 0.1;
    // jumps[1] = 0.15;
    // jumps[2] = 0.2;
    // jumps[3] = 0.3;    
    // jumps[4] = 0.4;
    // jumps[5] = 0.5;
    // jumps[6] = 0.6;
    // jumps[7] = 0.65;    
    // jumps[8] = 0.7;
    // jumps[9] = 0.8;
    // jumps[10] = 0.8565;
    // jumps[11] = 0.9;
    // jumps[2] = 0.6;
    // jumps[3] = 0.7;

    Geo well(x_l, jumps, x_r);
    //well = Geo();

    const Real h_ini = 0.1;
    const int nb_pt = 8; 
    
    // functions (B and mean)
    

    //const Statistics stat("Blakemore",0.27);
    const Statistics stat("Blakemore",0.27);

    const PhysicalData data(well, stat);

    const std::string flux_type_act = "activity"; // "expfittact"; // 

    const Sch_functions sch_fct_sg("SG","ari");
    Convergence_test(well,  data, sch_fct_sg, flux_type_act, h_ini , nb_pt, "SG" );
    //one_mesh(); 
    const Sch_functions sch_fct_b_harmo("B_harmo","ari");
    Convergence_test(well,  data, sch_fct_b_harmo, flux_type_act, h_ini , nb_pt, "harmo" );   
    
    const std::string flux_type_expf = "expfittact"; // "activity"; // 

    const Sch_functions sch_fct_harmo("SG","harmo");
    Convergence_test(well,  data, sch_fct_harmo, flux_type_expf, h_ini , nb_pt, "ExpF_SG_harmo" );

    const Sch_functions sch_fct_sgharmo("SG","ari");
    Convergence_test(well,  data, sch_fct_sgharmo, flux_type_expf, h_ini , nb_pt, "ExpF_SG_ari" );
    
    const Sch_functions sch_fct_harmoharmo("B_harmo","harmo");
    Convergence_test(well,  data, sch_fct_harmoharmo, flux_type_expf, h_ini , nb_pt, "ExpF_harmo_harmo" );

    const std::string flux_type_sedan = "SEDAN"; // "activity"; // 

    Convergence_test(well,  data, sch_fct_sg, flux_type_sedan, h_ini , nb_pt, "SEDAN_SG" );
    Convergence_test(well,  data, sch_fct_b_harmo, flux_type_sedan, h_ini , nb_pt, "SEDAN_harmo" );
}



int one_mesh()
{   
    //
    //geometry
    //

    PointsArrayType jumps;
    jumps.resize(4);
    jumps[0] = 0.3;
    jumps[1] = 0.45;
    jumps[2] = 0.6;
    jumps[3] = 0.75;
    // jumps.resize(4);
    // jumps[0] = 0.4;
    // jumps[1] = 0.6;
    Point x0 = 0.;
    Point x1 = 1.;
    Geo well(x0, jumps, x1);

    //Meshes 
    const Mesh mesh(0.1,well);

    mesh.file_cell_pos("primal");
    mesh.file_edge_pos("primal");  
    mesh.file_diamond_center_pos("primal");

    const Mesh dual_msh = get_dual_mesh(mesh);
    dual_msh.file_cell_pos("dual");
    dual_msh.file_edge_pos("dual");  
    dual_msh.file_diamond_center_pos("dual");

    //Physical data
    PhysicalData data(well, Statistics("Boltzmann", 0.27 ));
    BandEdgeType E_ = data.E;
    Discrete_u E(  E_ , mesh);   
    E.set_from_fonction(E_, 3);
    
    BandEdgeType E_WIAS = get_E_WIAS_style(E_,dual_msh,3);
       
    Discrete_u E_reg(  E_WIAS , dual_msh);
    std::cout << " test value left  " << E_(1.) << std::endl;
    std::cout << " test value left reg  " << E_WIAS(1.) << std::endl;

    E.file_visu_cst("energyband");
    E.file_visu_recons("energyband", 0, 200);
    
    E_reg.set_from_fonction(E_WIAS, 3);
    E_reg.file_visu_cst("energyband_WIAS");
    E_reg.file_visu_recons("energyband_WIAS", 0, 200);  
    
    std::cout << " test value right  " << E_(2.) << std::endl;
    std::cout << " test value right reg  " << E_WIAS(2.) << std::endl;


    // functions (B and mean)

    const std::string flux_type = "activity";

    Sch_functions sch_fct("SG","ari");
    
    Discrete_u c_cl=Discrete_u(mesh);
    c_cl = sch_sta_classic(mesh, data, sch_fct, flux_type);
    c_cl.file_visu_recons("classical",1,100);
    c_cl.file_visu_cst("cla");
    
    Discrete_u c_reg=Discrete_u(dual_msh);
    c_reg = sch_sta_reg(dual_msh, data, sch_fct,flux_type ); 
    c_reg.file_visu_recons("reg",1,100);
    c_reg.file_visu_cst("reg");
 
}

int debug()
{   
    double x = 0.5;
    std::cout << x << std::endl; 
    std::cout << "\nC'est parti pour le debuguage, youpi !!" << std::endl;
    PointsArrayType jumps;
    // jumps.resize(10);
    // jumps[0] = 0.4;
    // jumps[1] = 0.5;
    // jumps[2] = 0.55;
    // jumps[3] = 0.81;
    // jumps[4] = 0.82;
    // jumps[5] = 0.96347;
    // jumps[6] = 1.3;
    // jumps[7] = 1.5; 
    // jumps[8] = 1.52; 
    // jumps[9] = 1.55;
    jumps.resize(2);
    jumps[0] = 0.4;
    jumps[1] = 0.6;
    Point x0 = 0.;
    Point x1 = 1.;
    Geo simple(x0, jumps, x1);
    /*
    std::cout << "Nombre de reg: "<< simple.nb_reg() << std::endl;
    std::cout << "Lenght "<< simple.total_lenght() << std::endl;   
    std::cout << " 0.51 est dans la reg 1 ? "<< simple.is_in_reg(0.51,1) << std::endl;
    std::cout << " region de 0.5 "<< simple.which_reg(0.5) << std::endl;
    std::cout << " 1.51 est dans la reg 4 ? "<< simple.is_in_reg(0.51,1) << std::endl;
    std::cout << " region de 1.3 "<< simple.which_reg(1.3) << std::endl;
    std::cout << " region de 1.9 "<< simple.which_reg(1.9) << std::endl;
    std::cout << " region de 0.7 "<< simple.which_reg(0.7) << std::endl; 
    std::cout << " 0.4 est un saut ?  "<< simple.is_on_jump(0.4) << std::endl;
    std::cout << " 0.1 est un saut ?  "<< simple.is_on_jump(0.1) << std::endl;
    std::cout << " 0.1 est un bord ?  "<< simple.is_boundary(0.1) << std::endl;    
    std::cout << " 0. est un bord ?  "<< simple.is_boundary(0.) << std::endl;
    std::cout << " 1.85 est un bord ?  "<< simple.is_boundary(1.85) << std::endl;
    std::cout << " 1.85 est un saut ?  "<< simple.is_on_jump(1.85) << std::endl;
    std::cout << " 2. est un bord ?  "<< simple.is_boundary(2.) << std::endl; 
    */
    Mesh mesh(0.02,simple);

    mesh.file_cell_pos("primal");
    mesh.file_edge_pos("primal");  
    mesh.file_diamond_center_pos("primal");
     
    Mesh dual_msh = get_dual_mesh(mesh);
    dual_msh.file_cell_pos("dual");
    dual_msh.file_edge_pos("dual");  
    dual_msh.file_diamond_center_pos("dual");
    //Edge sigma =mesh.edge(2);
    //std::cout << " Normale par rapport à 0 "<< sigma.normal(0.) << std::endl;
    // std::cout << " Normale par rapport à 1 "<< sigma.normal(1.) << std::endl;    
    // std::cout << " Ks "<< mesh.cell_idx_Ks(2,2) << std::endl;
    // std::cout << " Ks "<< mesh.cell_idx_Ks(5,2) << std::endl;  
    // std::cout << " Ks "<< mesh.cell_idx_Ks(1,0) << std::endl;
    // std::cout << " Normale par rapport à 1 "<< sigma.normal(1.) << std::endl;

    PhysicalData data(simple, Statistics("Blakemore", 0.27 ));

    BandEdgeType E_ = data.E;
    Discrete_u E(  E_ , mesh);   
    E.set_from_fonction(E_, 3);    


    
    BandEdgeType E_WIAS = get_E_WIAS_style(E_,dual_msh,3);
    
       
    Discrete_u E_reg(  E_WIAS , dual_msh);
    std::cout << " test value left  " << E_(x0) << std::endl;
    std::cout << " test value left reg  " << E_WIAS(x1) << std::endl;

    
  
    std::cout << " Pos left edge   "<< (mesh.edge(mesh.number_edges()-1)).position()  << std::endl;
    if(2.==(mesh.edge(mesh.number_edges()-1)).position() ){std::cout << " Pos left edge comp OK  "  << std::endl;}

    std::cout << " Pos left edge comp diff  "  << 2.-(mesh.edge(mesh.number_edges()-1)).position()<<std::endl;
    std::cout << " test value left reg  "<< E_( (mesh.edge(mesh.number_edges()-1)).position() ) << std::endl;
    
    E.file_visu_cst("energyband");
    E.file_visu_recons("energyband", 0, 200);
    std::cout << " test value " << E.Recons(3)(2.) << std::endl;
    
    E_reg.set_from_fonction(E_WIAS, 3);
    
    E_reg.file_visu_cst("energyband_WIAS");
    E_reg.file_visu_recons("energyband_WIAS", 0, 200);

    std::cout << " test" << std::endl;
    Sch_functions sch_fct("SG","ari");
    std::cout << " mean" << sch_fct.m(1.,3.) << std::endl;
    std::cout << " d_mean" << sch_fct.d1_m(1.,3.) << std::endl;
    
    std::cout << sep << std::endl;
    std::cout << "About the scheme now" << std::endl;
    
    Discrete_u c_1 = Discrete_u(mesh);

    
    // for(int iE = 0; iE < mesh.number_edges(); iE++ ){
    //     std::cout << "Transissibility of edge " << iE <<" is:  " << fc.Tau_s(iE) << std::endl;
    //     std::cout << "Inverse of the transissibility " << iE <<" is:  " << 1./fc.Tau_s(iE) << std::endl;
    // }
    std::cout << " test 1 " << std::endl;
    // Activity_Flux *flux(0); 

    //Flux *flux= new Activity_Flux(mesh,fc,sch_fct);    
    
    //Activity_Flux *flux= new Activity_Flux(mesh,fc,sch_fct);


    //FluxType flux( new Activity_Flux(mesh,fc,sch_fct) );
     




    // std::cout << " test 2" << std::endl;
    // std::cout << " Multiple meshes ? " << &ptr_flux->fix_contr.m_mesh << std::endl;
    // std::cout << " Multiple meshes ? " << &fc.m_mesh << std::endl;
    // std::cout << " Multiple meshes ? " << &mesh << std::endl;
    
    // std::cout << " Multiple fluxes ? " << ptr_flux << std::endl;
    // std::cout << " Multiple fluxes ? " << &actflux << std::endl;    
    
    // std::cout << " Adress ? " << &actflux.fix_contr << std::endl;
    // std::cout << " Adress ? " << &(ptr_flux->fix_contr) << std::endl;

    
    // std::cout << " Number unkonw ? " << mesh.number_cells() << std::endl;
    // std::cout << " Number unkonw ? " << fc.m_mesh.number_cells() << std::endl;
    // std::cout << " Number unkonw ? " << ptr_flux->fix_contr.m_mesh.number_cells() << std::endl;
    //std::cout << " Number unkonw ? " << actflux.fix_contr.m_mesh.number_cells() << std::endl;

    // std::cout << " Number bounds? " <<  ptr_flux->fix_contr.f_K(0) << std::endl;
    
    //std::cout << " A typical flux ??" << flux.F_Ks(0,0) << std::endl;

    // NM.initialize_method(c_1,c_1);  
    // NM.enforce_boundary_values();
    // NM.actualize_flux();
    // NM.compute_goal_function_G();
    // NM.Gaudeul_test(5,3);
    
    // std::cout << " A typical flux ??" << flux.F_Ks(0,0) << std::endl;
    // std::cout << " A typical flux ??" << flux.F_Ks(0,1) << std::endl;
    
    B_Fct B("SG");
    // B.debug_test_diff(10); 
    // std::cout << " B(0) " << B.B(0.) << std::endl;
    // std::cout << " B'(0) " << B.dB(0.) << std::endl;


    ////////////////////
    ////////////////////
    //Discrete data  
    ////////////////////
    ////////////////////

    
 
    //BandEdgeType E_ = data.E;
    //Discrete_u E(  E_ , mesh);   
    E.set_from_fonction(E_, 3);
 

    RealFctType g_fct=[data] (const Real & s) -> Real {
        return data.stat.g(s);
    };

    Discrete_u Phi = Discrete_u(data.Phi,mesh); 
    Phi.set_from_fonction(data.Phi,3);

    const Fixed_contrib fc = Fixed_contrib(mesh,3,data);

    //Discrete_u E(  E_ , mesh);    
    //E.set_from_fonction(E_, 3);


    const Point x_lb = mesh.get_left_boundary_pos();
    const Point x_rb = mesh.get_right_boundary_pos();
    const Real c_eq_left = data.stat.g( -data.Phi(x_lb) - data.E(x_lb));
    const Real c_eq_right = data.stat.g( -data.Phi(x_rb) - data.E(x_rb));
    Discrete_u c_eq = (-1.*(Phi + E));
    c_eq.Apply_function(g_fct);

    std::cout << " Equilibrium " << std::endl;
    c_eq.print_values_debug(); 


    ////////////////////
    ////////////////////
    //Newton's method 
    ////////////////////
    ////////////////////


    std::cout << " Ex pot ??? \n " << std::endl;
    std::cout << " Val \t " << data.stat.expot(1.) << std::endl;
    std::cout << " Val \t " << data.stat.expot_fct()(1.) << std::endl;

    std::cout << " Ex pot ??? \n " << std::endl;

    
    Flux flux(mesh,fc,sch_fct,"SEDAN");


    //Creation of the Newton method
    Newton_method NM = Newton_method(mesh, fc,  flux);

    //Setting the boundary values (goal and equilibrium)
    NM.set_eq_boundary_values( c_eq_left , c_eq_right );
    NM.set_goal_boundary_values(data.c_b(x_lb),data.c_b(x_rb) );

    NM.initialize_method(c_eq,Phi); // method initialised with the quilibrium 
    std::cout << " Test1.0 " << std::endl;
    Real s =0.; // continuation parameter
    Real ds = 1.;
    std::vector<Real> List_s;
    List_s.push_back(s);
    
    bool Method_converges = false; 

    std::vector<int> Nb_iter_Newton;
    std::vector<int> Nb_resol_cumul;
    Nb_iter_Newton.push_back(0);
    Nb_resol_cumul.push_back(0);
    int n_methods = 0;
    int n_resol = 0;   


    NM.initialize_method(c_1,c_1);      
    std::cout << " Test1.1 " << std::endl;

    NM.enforce_boundary_values();    
    std::cout << " Test1.2 " << std::endl;

    NM.actualize_flux();
    std::cout << " Test1.3 " << std::endl;

    NM.compute_goal_function_G();
    std::cout << " Test1.4 " << std::endl;

    NM.Gaudeul_test(5,3);


    /*
    while( (s < 1. - tol_stopping ) && (ds > ds_crit )){

        // choice of the jumps in the continuation parameter
        Real max_step = std::min(1. - s, 1.);
        ds = std::min(ds, max_step);

        n_methods += 1;
           
        do{ //boucle newton  adapatative continuation strategy        
            
            
            NM.set_continuation_parameter(s + ds);
            
            std::cout << "Newton method number "<< n_methods <<" with ds = "<< ds << ": go! " << std::endl;
            Method_converges = NM.launch_method(); 

            n_resol += NM.get_number_iteration();

            if(Method_converges){
                s += ds;
                List_s.push_back(s);
                std::cout << "Computation of the solution with continuation parameter s = \t" << s << " :  \t OK"<< std::endl;
                ds = ds * (1. + 0.8);

                Nb_iter_Newton.push_back(NM.get_number_iteration());
                Nb_resol_cumul.push_back(n_resol);

            } //if Newton CV
            else{
                std::cout << "Computation of the solution with continuation parameter s = \t" << s + ds << " :  \t Failure"<< std::endl;
                std::cout << "Reduction of the continuation parameter step"<< std::endl;
                ds = 0.5 * ds;
            }

        }while(!Method_converges && (ds > ds_crit )); // boucle newton avec pas temps adaptatif

    } //boucle on s 

    if( (ds< ds_crit) && (s < 1.- tol_stopping ) ){
        std::cout << " \n \n "<< std::endl;
        std::cout << " !!!!!!!!!!!!!!!!!!! !!!!!!!!!!!!!!!!!!!  !!!!!!!!!!!!!!!!!!! " << std::endl;
        std::cout << "Current continuation parameter step : \n" << ds << std::endl;
        std::cout << "Resolution failure ! " << std::endl;
        return 0;
    } // if effective continuation parameter step becomes too small

    std::cout << "Computation of the stationnary solution : DONE !" << std::endl;

    Discrete_u c_sol = NM.get_c();

    c_sol.file_visu_recons("sol",0,100); 



    std::cout << "Done ! " << std::endl; 
    */   


    // MobilityType fct =[](const Point & x) -> Real{
    //     return x + exp(x) + log(1+x) - 1/(1+2.*x+x*x);     
    // };    
    
    
    // MobilityType prim =[](const Point & x) -> Real{
    //     return 0.5*x*x + exp(x)-2 + 1/(1+x);     
    // };
    
    // MobilityType primprim =[](const Point & x) -> Real{
    //     return x*x*x/6. + exp(x) - 2*x-1 + log(1+x);     
    // }; 
    
    // MobilityType aprox_prim =[fct,simple](const Point & x) -> Real{
    //     return Primitive_discontinuous(fct,x, simple,3);     
    // };  
    
    // MobilityType aprox_primprim =[aprox_prim,simple](const Point & x) -> Real{
    //     return Primitive_discontinuous(aprox_prim,x, simple,3);
    // };

    // std::srand(std::time(nullptr));
    // for(int i = 0; i< 20; i++){
    //     const Real x =  0.01 / (std::rand() % 1001);
    //     std::cout << "Values: \t"<< prim(x) - aprox_prim(x) << std::endl;
    //     std::cout << "Valuesprim: \t"<< primprim(x) - Primitive_value(prim,0.,x,3) << std::endl;        
    //     std::cout << "Valuesprimprim: \t"<< primprim(x) - aprox_primprim(x) << std::endl;

    // }



//     BandEdgeType prim_expE; 
//     prim_expE = [data,simple](const Point & x) -> Real{
//         const int reg_x =  simple.which_reg(x); 

//         Real prim = 0;
//         for(int reg =0; reg < reg_x; reg++){
//             const Point x_l = simple.position_jump(reg);
//             const Point x_r = simple.position_jump(reg+1);
//             prim += (x_r-x_l) * exp(data.E(0.5 * (x_l + x_r))); 
//         }
//         const Point x_l = simple.position_jump(reg_x);
//         prim += (x-x_l) * exp(data.E(0.5 * (x_l + x))); 
//         return prim;
//     };

//         std::cout << "Values prim   \t"<<  prim_expE(0.) << std::endl;
//         std::cout << "Values prim   \t"<<  data.prim_omega_inv(0.) << std::endl;

//     int n = 10;
//     Real h = 1./n;
//     for(int i =0; i<n; i++){
//         Point x = i*h;
//         std::cout << "Values omega: \t"<< data.omega(x) - exp(-data.E(x)) << std::endl;
//         std::cout << "Values prim omega: \t"<< prim_expE(x) - data.prim_omega_inv(x) << std::endl;
//     } 
//     for(int i =0; i<n; i++){
//         Point x = i*h;
//         std::cout << "Values prim Foveromega: \t"<< data.prim_F_over_omega(x)  << std::endl;
//         std::cout << "Values prim F: \t"<< data.ex_prim_F(x) - data.prim_F(x) << std::endl;
//     } 
// 
}

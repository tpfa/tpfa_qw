#ifndef SCHEME_H
#define SCHEME_H

#include "DiscreteData.h"


typedef std::function<Real(const Real &)> RealFctType;
typedef std::function<Real(const Real &,const Real & )> MeanFctType;

class B_Fct
{
    public:
    B_Fct();
    B_Fct(const std::string & type_B);
    Real B(const Real & x) const; 
    Real dB(const Real & x) const;

    void debug_test_diff( const int &  nb_point);

    private:
    RealFctType m_B_func;
    RealFctType m_dB_func;

};

class Mean_Fct
{
    public:
    Mean_Fct();
    Mean_Fct(const std::string & type_mean);
    Real m(const Real & x, const Real y) const; 
    Real d1_m(const Real & x, const Real y) const;
    Real d2_m(const Real & x, const Real y) const;


    private:
    MeanFctType m_mean;
    MeanFctType m_d1_mean;
    MeanFctType m_d2_mean;


};


class Sch_functions
{
    public:
    Sch_functions();
    Sch_functions(const std::string & type_B, const std::string & type_m );
    Real B(const Real & x) const;
    Real dB(const Real & x) const;

    Real m(const Real & x, const Real y) const; 
    Real d1_m(const Real & x, const Real y) const;
    Real d2_m(const Real & x, const Real y) const;

    private:
    std::string m_name_B;
    std::string m_name_m;
    Mean_Fct m_mean; 
    B_Fct m_B;
}; 

class Fixed_contrib //contributions which do not depend on time / iteration of the newton method
{
    public:
    Fixed_contrib();
    Fixed_contrib(const Mesh & msh, const int & order, const PhysicalData & data);
    
    Statistics stat;
    
    void set_discrete_doping(const ReconstructionType & f_ );
    void set_discrete_BEE(const ReconstructionType & E_ );
    void set_discrete_mobility(const ReconstructionType & mob_ );


    Real Tau_s(const int & iE) const;
    Real f_K(const int & iK) const;
    Real E_K(const int & iK) const;
    Real omega_K(const int & iK) const;
    Real mob_K(const int & iK) const;
    Discrete_u get_discrete_BEE() const; 
    Discrete_u get_discrete_omega() const; 
    
    
    private:
    Mesh m_mesh;
    int m_order_interpolant;
    std::vector<Real> m_transmi;
    SolutionVectorType m_f;
    Discrete_u m_omega_partial;
    Discrete_u m_Ed; 
    SolutionVectorType m_mob;
};


class Flux //  class for flux
{
    public:

    Flux(); 
    Flux(const Mesh & mesh, const Fixed_contrib & contribs, const Sch_functions & flux_fct, const std::string & type_flux );

    Real F_Ks(const int & iK, const int & iE) const ;
    
    Real dL_F_Ks(const int & iK, const int & iE, const int iL) const; 

    void set_concentration(const Discrete_u & c); 
    void set_potential(const Discrete_u & Phi); 
    
    void print_nb_cells() const; 
    

 
    protected:
    std::string m_type_flux;
    Fixed_contrib fix_contr;
    Mesh m_mesh;
    Sch_functions m_flux_fct;
    Statistics m_stat;
    Discrete_u  m_c;
    Discrete_u  m_Phi;
    Discrete_u m_omega;
    Discrete_u  m_B_arg; //argument of the B function, E + Phi 
    Discrete_u  m_u_partial; // u = h(c) * exp(E), used for the partial exp fitt scheme
    Discrete_u  m_inv_activity_partial; // beta = c / u, used for the partial exp fitt scheme
    Discrete_u  m_expot; //  the excess chimical potential, used for the SEDAN scheme
    Discrete_u  m_B_arg_sedan; //argument of the B function, E + Phi + expot 

};

// class Activity_Flux : public Flux
// {
//     public:   

//     Activity_Flux(const Mesh & mesh, const Fixed_contrib & contribs, const Sch_functions & flux_fct);
    
//     Real F_Ks(const int & iK, const int & iE)  const;
//     Real dL_F_Ks(const int & iK, const int & iE, const int iL) const;
    
//     //void print_nb_cells() const; 
   
//     /*
//     private:
//     Fixed_contrib fix_contr;
//     Mesh m_mesh;
//     Sch_functions m_flux_fct; 
//     Statistics m_stat;
//     Discrete_u  m_c;
//     Discrete_u  m_Phi;
//     Discrete_u  m_B_arg; //argument of the B function, E + Phi
//     */
// };

class Newton_method 
{
    public:  
    
    Newton_method();
    Newton_method( const  Mesh & mesh,const Fixed_contrib & contribs ,   Flux & flux);
    Newton_method( const Mesh & mesh,const Fixed_contrib & contribs , Flux & flux, const bool & complete_infos, const int & max_iterations, const Real & stopping_crit);
    ~Newton_method();

    void set_eq_boundary_values(const Real & c_eq_left, const Real & c_eq_right );
    void set_goal_boundary_values(const Real & c_goal_left, const Real & c_goal_right );

    void set_continuation_parameter(const Real & s);

    void initialize_method(const Discrete_u & c_ini,const Discrete_u & Phi_ini ); 

    void enforce_boundary_values();
    void actualize_flux();
    void compute_goal_function_G(); 
    void get_bounds_conformity(); 
    void compute_goal_norm(); 
    void compute_relative_res_norm() ; 

    void launch_iteration();

    void print_infos_iteration() const;
    
    bool launch_method(); //return true if the method converges, false if the method did not ...

    Discrete_u get_c() const; 
    int get_number_iteration() const;

    void Gaudeul_test(const int & number_base_point, const int & number_dir );


    private:
    Flux m_flux;
    Mesh m_mesh;
    int m_nb_unkn;
    Fixed_contrib m_fix_contr;  
    
    Real m_c_eq_left;
    Real m_c_eq_right;
    Real m_c_goal_left;
    Real m_c_goal_right;

    Real m_s;
    Real m_c_lb;
    Real m_c_rb;
    
    Discrete_u m_Phi;   
    Discrete_u m_c;
    Discrete_u m_res;
    SolutionVectorType m_G; 

    
    Real m_norm_goal;
    Real m_norm_rel_residue;

    int m_nb_it;
    bool m_is_CV;
    bool m_can_continue;
    bool m_goal_is_small;
    bool m_res_is_small;
    bool m_res_is_very_small;
    bool m_bounds;

    int m_max_it;
    Real m_stopping_crit;
    bool m_print_info;

};

#endif

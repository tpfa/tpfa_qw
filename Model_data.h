#ifndef MODEL_DATA_H
#define MODEL_DATA_H


#include <boost/integer/static_min_max.hpp>
#include <boost/math/constants/constants.hpp>
#include <boost/numeric/ublas/vector.hpp>


#include "eigen3/Eigen/SparseCore"
#include "eigen3/Eigen/SparseLU"
#include "eigen3/Eigen/Core"

#include <eigen3/unsupported/Eigen/SparseExtra>


#include <vector>
#include <functional>
#include <array>
#include <iostream>
#include <fstream>
#include <string>
#include <string.h>
#include <cmath>


// type definitions

typedef double Point; //type for real corresponding to points on the domain 
typedef double Real; //type for real corresponding to quantities 
typedef std::vector<Point> PointsArrayType;


typedef std::function<Real(const Point &)> PotentialType;
typedef std::function<Real(const Point &)> BandEdgeType;
typedef std::function<Real(const Point &)> DopingType;
typedef std::function<Real(const Point &)> PermitivityType;
typedef std::function<Real(const Point &)> MobilityType;

typedef std::function<Real(const Point &)> LiftingBoundaryType;
typedef std::function<Real(const Point &)> InitialDataType;

typedef std::function<Real(const Real &)> RealFctType;
typedef std::function<Real(const Point &)> ReconstructionType;




// some usefull fonctions







Real integral_approx(const ReconstructionType & fct, const Real & a_l, const Real & a_r, const int & order );
Real mean_val_approx(const ReconstructionType & fct, const Real & a_l, const Real & a_r, const int & order );
Real Primitive_value(const ReconstructionType & fct, const Point & x_0, const Point & x, const int & order);


// Physical constants 

struct CstPhy
{                   
    // SI units
    const Real K  = 1.0;        // Kelvin
    const Real J  = 1.0;        // Joule
    const Real A  = 1.0;        // Ampere
    const Real V  = 1.0;        // Volt
    const Real m  = 1.0;        // meter
    const Real s  = 1.0;        // seconds
    const Real C  = A * s;      // Coulomb
    const Real kg = 1.0;        // kilogramm
    const Real Hz = 1. / s;     // Hertz
    const Real kHz = 1.0e3 * Hz;// Kilohertz


    // scaled SI units
    const Real cm = 1.0e-2 * m;
    const Real mm = 1.0e-3 * m;
    const Real micro_m = 1.0e-6 * m;
    const Real nm = 1.0e-9 * m;

    const Real ms = 1.0e-3 * s;
    const Real micro_s = 1.0e-6 * s;
    const Real ns = 1.0e-9 * s;
    const Real ps = 1.0e-12 *s;



    // some (universal) physical constants

    const Real kB              = 1.38064852e-23;        // JK^{-1}     --- Boltzmann constant
    const Real Planck_constant = 6.62607015e-34;        // Js          --- Planck constant
    const Real m_elec          = 9.1093837015e-31;      // kg          --- electron rest mass
    const Real q               = 1.602176634e-19;       // C           --- elementary charge
    const Real eps_0           = 8.8541878176e-12;      // (A*s)/(V*m) --- absolute dielectric permittivity of classical vacuum
    const Real eV              = q * V ;                     // (A*s)/(V*m) --- absolute dielectric permittivity of classical vacuum


};


// Geometry of the problem

class Geo
{   
    public:
    Geo();
    Geo(const Point & x_left,const  PointsArrayType & jumps, const Point & x_right);

    int nb_reg() const;
    bool is_in_reg(const Point & x, const int & reg) const;
    int which_reg(const Point & x) const;
    bool is_on_jump(const Point & x) const;
    bool is_boundary(const Point & x) const;
    double total_lenght() const ; 
    
    Point position_jump(const int & k) const; 


    private:
    int m_number_reg;
    Point m_left_boundary;
    Point m_right_boundary;
    PointsArrayType m_jumps_positions; //including boundary

}; 


Real Primitive_discontinuous(const ReconstructionType & fct,  const Point & x,const  Geo & geometry,  const int & order);


class Statistics
{
    public:
    Statistics();
    Statistics(const std::string & stat_name , const Real & gamma);
    Real h(const Real & s) const ;
    Real dh(const Real & s) const ;
    Real H(const Real & s) const;
    Real g(const Real & s) const ;
    Real dg(const Real & s) const ;
    bool is_Ih_valued(const Real & s) const; 
    
    Real nb_bounds;
    std::vector<Real> Bounds; 

    Real a(const Real & s) const ;
    Real da(const Real & s) const ;
    Real Beta(const Real & s) const ;
    Real dBeta(const Real & s) const ;    
    Real expot(const Real & s) const ;
    Real dexpot(const Real & s) const ;
    
    RealFctType h_fct() const;
    RealFctType expot_fct() const;

    private:
    RealFctType m_h;
    RealFctType m_dh ;
    RealFctType m_H;
    RealFctType m_g;
    RealFctType m_dg ;
    RealFctType m_expot ;
    std::function<bool(const Real &)> m_bounds;
};

class PhysicalData
{   
    
    public:
    PhysicalData();
    PhysicalData(const Geo & geometry, const Statistics & statistics);
    
    Statistics stat;
    
    BandEdgeType E;   
    PotentialType Phi;
    DopingType f;
    MobilityType mob;
    
    MobilityType omega;
    
    LiftingBoundaryType c_b;
    InitialDataType c_exact;  


    private:
    Geo m_geo;

};

#endif

#include "Model_data.h"


#include <vector>
#include <functional>
#include <array>
#include <iostream>
#include <fstream>
#include <string>
#include <string.h>
#include <cmath>


#ifndef tol_jump
#define tol_jump 5.*std::pow(10., -15)
#endif

#ifndef tol_extend_boundary
#define tol_extend_boundary 5.*std::pow(10., -5)
#endif

#ifndef size_primitive
#define size_primitive 5.*std::pow(10., -5)
#endif





// type definitions

typedef double Point; //type for real corresponding to point on the domain
typedef double Real; //type for real corresponding to quantities 

typedef std::vector<Point> PointsArrayType;
typedef std::function<bool(const Point &, const int &)> IsInRegType;
typedef std::function<int(const Point &)> WhichRegType;




//some quadrture routines

Real integral_approx(const ReconstructionType & fct, const Real & a_l, const Real & a_r, const int & order )
{
    int effective_order = 1;  
    Real I;
    const Point x_m = 0.5 * (a_l + a_r);
    if(order==2){
        std::cout <<"WARNING : you asked for a quadrature of order" << order <<" which is not implemented (yet ?) !! \n"  <<  std::endl;
        std::cout <<"The interpolation used will be of order 1 (barycentric value) instead \n"  <<  std::endl;
        effective_order = 1 ;
    }
    if(order > 3 ){
        std::cout <<"WARNING : you asked for a interpolation of order" << order <<" which is not implemented (yet ?) !! \n"  <<  std::endl;
        std::cout <<"The interpolation used is of order 3 (Simpson formula) instead \n"  <<  std::endl;
        effective_order = 3 ;
    }   
    if(effective_order == 1){ I = fct(x_m);}
    if(effective_order == 3){ I = (1./6.)*(fct(a_l) + 4.* fct(x_m) + fct(a_r) ); }
    return (a_r-a_l) * I;
}

Real mean_val_approx(const ReconstructionType & fct, const Real & a_l, const Real & a_r, const int & order )
{
    int effective_order = 1;  
    Real M_v;
    const Point x_m = 0.5 * (a_l + a_r);
    if(order==2){
        std::cout <<"WARNING : you asked for a quadrature of order" << order <<" which is not implemented (yet ?) !! \n"  <<  std::endl;
        std::cout <<"The interpolation used will be of order 1 (barycentric value) instead \n"  <<  std::endl;
        effective_order = 1 ;
    }
    if(order > 3 ){
        std::cout <<"WARNING : you asked for a interpolation of order" << order <<" which is not implemented (yet ?) !! \n"  <<  std::endl;
        std::cout <<"The interpolation used is of order 3 (Simpson formula) instead \n"  <<  std::endl;
        effective_order = 3 ;
    }   
    if(effective_order <2){ M_v = fct(x_m);}
    if(effective_order == 3){ M_v = (1./6.)*(fct(a_l) + 4.* fct(x_m) + fct(a_r) ); }
    return  M_v;
}

Real Primitive_value(const ReconstructionType & fct, const Point & x_0, const Point & x, const int & order)
{
    Real prim = 0.;
    const Real l = x-x_0; 

    if(l >= 0){
        const int nb_quad = ( (int) l/size_primitive ) +1;
        const Real h_quad = l/ ( 1.* nb_quad); 
        for(int i = 0; i< nb_quad; i++ ){
            prim += integral_approx(fct, x_0 + i * h_quad,x_0 + (i+1) * h_quad, order );
        } // for i
    } 
    else{        
        const int nb_quad = ( (int) -l/size_primitive ) +1;
        const Real h_quad = -l/ ( 1.* nb_quad); 
        for(int i = 0; i< nb_quad; i++ ){
            prim += - integral_approx(fct, x_0 - (i+1) * h_quad, x_0 - i * h_quad, order );
        } // for i
    }
    return prim;

}



//Geometry, implementation 

Geo::Geo()
{
    m_number_reg = 1;
    m_left_boundary = 0.;
    m_right_boundary = 1.;
    PointsArrayType j_pos;
    j_pos.resize(2);
    j_pos[0]=0.;
    j_pos[1]=1.;
    m_jumps_positions = j_pos; //including boundary
}

Geo::Geo(const Point & x_left, const PointsArrayType & jumps,const  Point & x_right)
{
    m_number_reg = jumps.size()+1;
    m_left_boundary = x_left;
    m_right_boundary = x_right;
    PointsArrayType j_pos;
    j_pos.resize( m_number_reg + 1);
    j_pos[0]=x_left;
    for(int j=0;j < jumps.size(); j++ ){
        j_pos[j+1] = jumps[j];
        } // for j 
    j_pos[m_number_reg]= x_right;
    m_jumps_positions = j_pos; //including boundary
}

int Geo::nb_reg() const
{
    return m_number_reg;
}

bool Geo::is_in_reg(const Point & x, const int & reg) const
{   
    if(reg == 0){
        return (m_jumps_positions[reg]-tol_extend_boundary <= x) && (x <= m_jumps_positions[reg+1]);
    }
    else if(reg == m_number_reg-1){
        return (m_jumps_positions[reg] <= x) && (x <= m_jumps_positions[reg+1] + tol_extend_boundary);
    }
    else{
        return (m_jumps_positions[reg] <= x) && (x <= m_jumps_positions[reg+1]);
    }
}

int Geo::which_reg(const Point & x) const
{
    for(int reg = 0;m_number_reg; reg ++ ){
        if(is_in_reg( x, reg)){return reg; }
    } // for reg 
}

bool Geo::is_on_jump(const Point & x) const
{   
    for(int j = 1; j <m_number_reg; j ++ ){
        if( std::fabs(x - m_jumps_positions[j])< tol_jump ){return true; }
    }
    return false;
}

bool Geo::is_boundary(const Point & x) const
{   
    return (std::fabs(x - m_left_boundary) < tol_jump) || (std::fabs(x - m_right_boundary)<tol_jump); 
}

double Geo::total_lenght() const
{
    return m_right_boundary - m_left_boundary; 
}
Point Geo::position_jump(const int & k) const
{
    return m_jumps_positions[k]; 
} 

// computation of primitive of discontinuous fonction
Real Primitive_discontinuous(const ReconstructionType & fct,  const Point & x,const  Geo & geometry,  const int & order)
{
    const int reg_x =  geometry.which_reg(x); 
    Real prim = 0.;
    for(int reg =0; reg < reg_x; reg++){
        const Point x_l = geometry.position_jump(reg) + 0.0000000001;
        const Point x_r = geometry.position_jump(reg+1)- 0.0000000001;
        prim += Primitive_value(fct,x_l,x_r,order); 
    }
    const Point x_l = geometry.position_jump(reg_x)+ 0.0000000001;
    prim += Primitive_value(fct,x_l,x, order); 
    return prim;

}




//Statistics, implementation
Statistics::Statistics()
{
    m_h = [](const Real & s) -> Real{return log(s);};
    m_dh = [](const Real & s) -> Real{return 1./s;};
    m_H = [](const Real & s) -> Real{return  s * log(s) - s +1. ;};
    m_g = [](const Real & s) -> Real{return  exp(s);}; 
    m_dg = [](const Real & s) -> Real{return  exp(s);};
    m_bounds = [](const Real & s) -> bool {return  (s>0);};
    nb_bounds = 1;
    Bounds.resize(1);
    Bounds[0]=0.; 

}


Statistics::Statistics(const std::string & stat_name, const Real & gamma)
{
    if(stat_name =="Boltzmann"){
        m_h = [](const Real & s) -> Real{return log(s);};
        m_dh = [](const Real & s) -> Real{return 1./s;};
        m_H = [](const Real & s) -> Real{return  s * log(s) - s +1. ;};
        m_g = [](const Real & s) -> Real{return  exp(s);}; 
        m_dg = [](const Real & s) -> Real{return  exp(s);};        
        
        m_bounds = [](const Real & s) -> bool {return  (s>0);}; 
        nb_bounds = 1;
        Bounds.resize(1);
        Bounds[0]=0.;         
    }
    else if(stat_name =="Blakemore"){
        RealFctType prim_log = [](const Real & s) -> Real{return  s * log(s) - s +1. ;};
        
        m_h = [gamma](const Real & s) -> Real{return log(s/(1. - gamma *s));};
        m_dh = [gamma](const Real & s) -> Real{return 1./( s * (1.- gamma*s));};
        m_H = [gamma,prim_log](const Real & s) -> Real{return  prim_log(s) + (1./gamma) * prim_log(1. - gamma* s) ;};
        m_g = [gamma](const Real & s) -> Real{return  1./(gamma + exp(-s));}; 
        m_dg = [gamma](const Real & s) -> Real{return  exp(s)/std::pow(1. + gamma * exp(s),2.);};        
        
        m_bounds = [gamma](const Real & s) -> bool {return  (s>0) && (s< 1./gamma);};        
        nb_bounds = 2;
        Bounds.resize(2);
        Bounds[0]=0.; 
        Bounds[1]= 1./gamma; 
    }
    else{
        std::cout <<"You want to use the " + stat_name +"statistics  \n"  <<  std::endl;
        std::cout <<"This statistics is not implemented (yet ?) \n"  <<  std::endl;
        std::cout <<"Juste check the spelling ;)  \n"  <<  std::endl;
    }
    
}


Real Statistics::h(const Real & s) const 
{
    return m_h(s);
}

Real Statistics::dh(const Real & s) const 
{
    return m_dh(s);
}

Real Statistics::H(const Real & s) const
{
    return m_H(s); 
}

Real Statistics::g(const Real & s) const 
{
    return m_g(s);
}

Real Statistics::dg(const Real & s) const
{
    return m_dg(s); 
}

bool Statistics::is_Ih_valued(const Real & s) const
{
    return m_bounds(s);
}    
Real Statistics::a(const Real & s) const 
{
    return exp(h(s));
}

Real Statistics::da(const Real & s) const
{
    return dh(s) * a(s); 
} 

Real Statistics::Beta(const Real & s) const 
{
    return s / a(s);
}

Real Statistics::dBeta(const Real & s) const 
{
    return  (1. - s * dh(s) ) / a(s);
}

Real Statistics::expot(const Real & s) const 
{
    return h(s) - log(s);
}

Real Statistics::dexpot(const Real & s) const 
{
    return  dh(s) - 1./s;
}

RealFctType Statistics::h_fct() const
{
    return m_h;
}

RealFctType Statistics::expot_fct() const
{   
    RealFctType expot_fc = [this](const Real & s) -> Real{return this->h(s) - log(s);};
    return expot_fc;
}

//Physical data, implementation 
PhysicalData::PhysicalData()
{   
    stat = Statistics();
    
    E = [](const Point & x) -> Real{return 1.;};   
    Phi = [](const Point & x) -> Real{return 1.;};  
    f= [] (const Point & x) -> Real{return 0.;};
    mob = [] (const Point & x) -> Real{return 1.;};
    c_b = [] (const Point & x) -> Real{return 1.;};
    c_exact = [] (const Point & x) -> Real{return 1.;};

    m_geo =Geo();
}




// PhysicalData::PhysicalData(const Geo & geometry, const Statistics & statistics )
// {    // for Boltzmann statistics, loading term, no elec pot

//     stat = statistics;
    
//     const int nb_val =  geometry.nb_reg();
    
//     const Point x_bl = geometry.position_jump(0); 
//     const Point x_br = geometry.position_jump(nb_val);

    
//     std::vector<Real> values_energy;
//     std::srand(std::time(nullptr));
//     for(int reg = 0; reg < nb_val; reg++){
//             values_energy.push_back( (std::rand() % 11) - 5.);
//     }

//     const Point x_m = 0.5 * (x_bl + x_br)  ;

//     E = [values_energy,geometry](const Point & x) -> Real{
//         return 0. + 0.2 * values_energy[geometry.which_reg(x)];
//     };

//     BandEdgeType prim_expE; 
//     prim_expE = [this,geometry](const Point & x) -> Real{
//         const int reg_x =  geometry.which_reg(x); 
//         Real prim = 0;
//         for(int reg =0; reg < reg_x; reg++){
//             const Point x_l = geometry.position_jump(reg);
//             const Point x_r = geometry.position_jump(reg+1);
//             prim += (x_r-x_l) * exp(this->E(0.5 * (x_l + x_r))); 
//         }
//         const Point x_l = geometry.position_jump(reg_x);
//         prim += (x-x_l) * exp(this->E(0.5 * (x_l + x))); 
//         return prim;
//     };
//     const Real a = 2.;
//     const Real b = 2.;
//     const Real p = 1.;
//     const Real v = 1.;

    
//     Phi = [p,v](const Point & x) -> Real{return  0. ;}; 
    



//     omega = [this](const Point & x ) -> Real{ return exp( -(this->E(x))-(this->Phi(x)));};

//     f = [a,b,p] (const Point & x) -> Real{ 
//         return a * (2. + b*std::pow(x,b-1.) * exp(std::pow(x,b))*( 1. + b* (1. + std::pow(x,b)) ) ) ;
//     };  
//     //a*(exp(b*x) + std::pow(x,p)); };
    
//     mob = [] (const Point & x) -> Real{return 1.;};
//     c_b = [] (const Point & x) -> Real{return 1.;};   
    
//     const Real u_left = c_b(x_bl) /omega (x_bl); 
//     const Real u_right = c_b(x_br) / omega(x_br);
 
    
//     BandEdgeType F = [a,b,p] (const Point & x) -> Real{ 
//         return a * (2.*x + (1. + b*std::pow(x,b)) * exp(std::pow(x,b))) ;
//     };  //a  *(exp(b*x)/b + std::pow(x,p+1.)/(p+1.)); };
    
//     BandEdgeType prim_F = [a,b,p] (const Point & x) -> Real{ 
//         return  a * (x*x + x * exp(std::pow(x,b))) ;
//     };  //a  *(exp(b*x)/(b*b) + + std::pow(x,p+2.)/((p+1.)*(p+2.))); };
    
//     BandEdgeType prim_FexpE; 
//     prim_FexpE = [this,geometry,prim_F](const Point & x) -> Real{
//         const int reg_x =  geometry.which_reg(x); 
//         Real prim = 0;
//         for(int reg =0; reg < reg_x; reg++){
//             const Point x_l = geometry.position_jump(reg);
//             const Point x_r = geometry.position_jump(reg+1);
//             prim += exp(this->E(0.5 * (x_l + x_r))) * (prim_F(x_r)-prim_F(x_l)) ; 
//         }
//         const Point x_l = geometry.position_jump(reg_x);
//         prim += exp(this->E(0.5 * (x_l + x)))* (prim_F(x)-prim_F(x_l)); 
//         return prim;
//     };   
    
//     const Real alpha = (u_right - u_left  + prim_FexpE(x_br) ) / prim_expE(x_br);     
    

//     c_exact = [this, prim_expE,u_left, alpha,prim_FexpE] (const Point & x) -> Real{ 
//         return exp( - this->E(x)) * ( u_left + alpha * prim_expE(x) - prim_FexpE(x)  ) ;
//     };

//     m_geo =  geometry;
// }



PhysicalData::PhysicalData(const Geo & geometry, const Statistics & statistics )
{   // for Blakemore stat
    stat = statistics;

    const Real gamma = 1./ stat.Bounds[1];
   
    const int nb_val =  geometry.nb_reg();
    
    const Point x_bl = geometry.position_jump(0); 
    const Point x_br = geometry.position_jump(nb_val);

    
    std::vector<Real> values_energy;
    std::srand(std::time(nullptr));
    for(int reg = 0; reg < nb_val; reg++){
            values_energy.push_back( (std::rand() % 11) - 5.);
    }

    const Point x_m = 0.5 * (x_bl + x_br)  ;

    E = [values_energy,geometry](const Point & x) -> Real{
        return 1. + 0.1 * values_energy[geometry.which_reg(x)];
    };

    std::function<Real(const MobilityType & g ,const Point &)> prim_g_expE;
    prim_g_expE = [this,geometry](const MobilityType & prim_g, const Point & x) -> Real{

        const int reg_x =  geometry.which_reg(x); 
        Real prim = 0;
        for(int reg =0; reg < reg_x; reg++){
            const Point x_l = geometry.position_jump(reg);
            const Point x_r = geometry.position_jump(reg+1);
            prim += exp(this->E(0.5 * (x_l + x_r))) * (prim_g(x_r)-prim_g(x_l)) ; 
        }
        const Point x_l = geometry.position_jump(reg_x);
        prim += exp(this->E(0.5 * (x_l + x)))* (prim_g(x)-prim_g(x_l)); 
        return prim;
    };
    const Real a = 1.;
    const Real b = 3;
    const Real p = 10.;
    const Real v = 150.;

    const Real alpha = 12.;

    const Real c_bl = 1.; 

    
    Phi = [p,v](const Point & x) -> Real{
        return   p*x; //p*log(x + 1./v) ; //
    };     
    

    
    BandEdgeType prim_exp_tot =[p,v, alpha, gamma] (const Point & x) -> Real{
         return  exp((p - alpha *gamma )*x) / (p - alpha * gamma) ; 
         // std::pow(x+ 1./v,p+1)/(p+1.); //
    };

    
    omega = [this](const Point & x ) -> Real{ return exp( -(this->E(x))-(this->Phi(x)));};

    f = [] (const Point & x) -> Real{ return  0. ;};//a*(exp(b*x) + std::pow(x,p)); };
    
    mob = [] (const Point & x) -> Real{return 1.;};
    
    const Real u_left = stat.a(c_bl) /omega (x_bl); 
    
    BandEdgeType prim_1 =[prim_g_expE, prim_exp_tot] (const Point & x) -> Real{ 
        return prim_g_expE(prim_exp_tot,x) ; 
    };    
   
    BandEdgeType u =[u_left, prim_1, x_bl, alpha, gamma] (const Point & x) -> Real{ 
        const Real prefact = exp(alpha * gamma * x );
        return  prefact * (  u_left * exp(- alpha * gamma * x_bl) + alpha * prim_1(x)  ) ; 
    };    


    

    c_exact = [this, u] (const Point & x) -> Real{ 
        const Real om_x = this->omega(x);
        return this->stat.g(log(om_x * u(x))) ;};

    c_b = [this] (const Point & x) -> Real{return this->c_exact(x);};   

}



// PhysicalData::PhysicalData(const Geo & geometry, const Statistics & statistics )
// {   // for Boltzmann stat, exact sol with f = 0
//     stat = statistics;
    
//     const int nb_val =  geometry.nb_reg();
    
//     const Point x_bl = geometry.position_jump(0); 
//     const Point x_br = geometry.position_jump(nb_val);

    
//     std::vector<Real> values_energy;
//     std::srand(std::time(nullptr));
//     for(int reg = 0; reg < nb_val; reg++){
//             values_energy.push_back( (std::rand() % 11) - 5.);
//     }

//     const Point x_m = 0.5 * (x_bl + x_br)  ;

//     E = [values_energy,geometry](const Point & x) -> Real{
//         return 0.+ 0.2 * values_energy[geometry.which_reg(x)];
//     };

//     std::function<Real(const MobilityType & g ,const Point &)> prim_g_expE;
//     prim_g_expE = [this,geometry](const MobilityType & prim_g, const Point & x) -> Real{

//         const int reg_x =  geometry.which_reg(x); 
//         Real prim = 0;
//         for(int reg =0; reg < reg_x; reg++){
//             const Point x_l = geometry.position_jump(reg);
//             const Point x_r = geometry.position_jump(reg+1);
//             prim += exp(this->E(0.5 * (x_l + x_r))) * (prim_g(x_r)-prim_g(x_l)) ; 
//         }
//         const Point x_l = geometry.position_jump(reg_x);
//         prim += exp(this->E(0.5 * (x_l + x)))* (prim_g(x)-prim_g(x_l)); 
//         return prim;
//     };
//     const Real a = 1.;
//     const Real b = 3;
//     const Real p = 400.;
//     const Real v = 150.;

    
//     Phi = [p,v](const Point & x) -> Real{
//         return   p*x; //p*log(x + 1./v) ; //
//     };     
    
//     BandEdgeType prim_expPhi =[p,v] (const Point & x) -> Real{
//          return  exp(p*x) / p ; // std::pow(x+ 1./v,p+1)/(p+1.); //
//     };

    
//     omega = [this](const Point & x ) -> Real{ return exp( -(this->E(x))-(this->Phi(x)));};

//     f = [] (const Point & x) -> Real{ return  0. ;};//a*(exp(b*x) + std::pow(x,p)); };
    
//     mob = [] (const Point & x) -> Real{return 1.;};
//     c_b = [] (const Point & x) -> Real{return 0.5+1.5*x;};   
    
//     const Real u_left = c_b(x_bl) /omega (x_bl); 
//     const Real u_right = c_b(x_br) / omega(x_br);
 
    


//     BandEdgeType prim_1 =[prim_g_expE, prim_expPhi] (const Point & x) -> Real{ return prim_g_expE(prim_expPhi,x) ; };    
   



    
//     const Real alpha = (u_right - u_left  ) / prim_1(x_br);     
    

//     c_exact = [this, prim_1,u_left, alpha] (const Point & x) -> Real{ 
//         const Real om_x = this->omega(x);
//         return om_x * ( u_left + alpha * prim_1(x) ) ;};

// }



// PhysicalData::PhysicalData(const Geo & geometry, const Statistics & statistics )
// {   // Boltmann with loading term, and electrostatic potential --> not working :(
//     stat = statistics; 
//     m_geo =  geometry;
  
//     const int nb_val =  m_geo.nb_reg();
    
//     const Point x_bl = m_geo.position_jump(0); 
//     const Point x_br = m_geo.position_jump(nb_val);

    
//     std::vector<Real> values_energy;
//     std::srand(std::time(nullptr));
//     for(int reg = 0; reg < nb_val; reg++){
//             values_energy.push_back( (std::rand() % 11) - 5.);
//     }


//     E = [values_energy,this](const Point & x) -> Real{
//         return  0. + 0.5 * values_energy[this->m_geo.which_reg(x)];
//     };


//     const Real a = 1.;
//     const Real b = 3;
//     const Real p = 0.;
//     const Real v =1.;

    
//     Phi = [](const Point & x) -> Real{return 0. ;}; 

//     f = [] (const Point & x) -> Real{ return x*x*x ;};//a*(exp(b*x) + std::pow(x,p)); };
    
//     DopingType F = [this](const Point & x) -> Real{ return Primitive_discontinuous(this->f,x,this->m_geo,3);};
//     //DopingType prim_F = [F,x_bl,m_geo](const Point & x) -> Real{ return Primitive_value(F, x_bl, x,3 );};


    
//     mob = [] (const Point & x) -> Real{return 1.;};
//     c_b = [] (const Point & x) -> Real{return 1.+2.*x;};   
    
//     omega = [this](const Point & x ) -> Real{ return exp( -(this->E(x) + this->Phi(x)));};
    
//     MobilityType omega_inv = [this](const Point & x) -> Real{ return 1./this->omega(x);};    
    
//     MobilityType F_over_omega = [F,this](const Point & x) -> Real{ return F(x) *exp( this->E(x) + this->Phi(x)) ;};


//     MobilityType prim_omega_inv = [omega_inv,this](const Point & x) -> Real{ return Primitive_discontinuous(omega_inv,x,this->m_geo,3);};
    
//     MobilityType prim_F_over_omega = [F_over_omega,this](const Point & x) -> Real{ return Primitive_discontinuous(F_over_omega,x, this->m_geo ,3);};


//     const Real u_left = c_b(x_bl) /omega (x_bl);         
//     const Real u_right = c_b(x_br) / omega(x_br);   

//     const Real alpha = (u_right - u_left  + prim_F_over_omega(x_br) ) / prim_omega_inv(x_br);     

//     c_exact = [this,u_left, alpha,prim_omega_inv,prim_F_over_omega] (const Point & x) -> Real{ 
//         return this->omega(x) * ( u_left + alpha * prim_omega_inv(x) - prim_F_over_omega(x)  ) ;};

// }

